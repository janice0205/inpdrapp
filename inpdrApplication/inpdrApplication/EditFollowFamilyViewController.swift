//
//  EditFollowFamilyViewController.swift
//  inpdrApplication
//
//  Created by Janice on 21/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class EditFollowFamilyViewController: UIViewController {

    @IBOutlet weak var myTable: UITableView!
    
    var dictHeader = [String: String]()

    var alert = SweetAlert()
    var dictPicker = [Int: [String]]()
    var startIndex: Int?
    var listQuestion2 = [String]()
    var listQuestion3 = [String]()
    var followques = followup()
    var dictAnswer = [Int : Int]()
    var followable_id : Int?
    var type : String?
    var id : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(editWiderImpact))
        self.myTable.registerNib(UINib(nibName: "EditTableViewCell", bundle: nil), forCellReuseIdentifier: "editCell")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(dictAnswer.count > listQuestion2.count){
            return listQuestion2.count
        }else{
            return dictAnswer.count
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = myTable.dequeueReusableCellWithIdentifier("editCell") as! EditTableViewCell
        cell.answerText.text = dictPicker[indexPath.row + startIndex!]![dictAnswer[indexPath.row + startIndex!]!]
        cell.questionView.text = listQuestion2[indexPath.row]
        cell.pickerDataSource = dictPicker[indexPath.row + startIndex!]!
        cell.answerText.addTarget(self, action: #selector(AddFollowUpViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingDidEnd)
        cell.answerText.tag = indexPath.row
        return cell
    }
    
    func textFieldDidChange(sender: UITextField){
        let cell = sender.superview?.superview as! AddFollowTableViewCell
        dictAnswer[sender.tag + startIndex!] = cell.pickerValue
//        let  rowIndex = myTable.indexPathForCell(cell)?.row
//        selectedAnswerForRow[rowIndex!] = cell.answerValue
    }
    
    func editWiderImpact(){
        print(dictAnswer)
        if(dictAnswer.values.contains(-1)){
            alert.showAlert("Error", subTitle: "Please fill all questions", style: AlertStyle.Error)
        }else{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("editWider") as! EditFollowWiderViewController
            vc.dictHeader = dictHeader
            vc.dictPicker = self.dictPicker
            vc.startIndex = self.startIndex! + self.listQuestion2.count
            vc.listQuestion3 = self.listQuestion3
            vc.followques = followques
            vc.navigationItem.title = "Wider Impact"
            vc.dictAnswer = dictAnswer
            vc.followable_id = followable_id
            vc.type = type!
            vc.id = id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}
