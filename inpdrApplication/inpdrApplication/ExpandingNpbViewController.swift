//
//  ExpandingNpbViewController.swift
//  inpdrApplication
//
//  Created by Janice on 14/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class ExpandingNpbViewController: UIViewController {

    var ques = question()
    
    var alert = SweetAlert()
    var sectionTitleArray : NSMutableArray = NSMutableArray()
    var sectionContentDict : NSMutableDictionary = NSMutableDictionary()
    var arrayForBool : NSMutableArray = NSMutableArray()
    
    var dictHeader = [String: String]()
    
    var DicPatientInfo = NSMutableDictionary()
    var ListPatientInfo = [String]()
    var DicEnrollInfo = NSMutableDictionary()
    var ListEnrollInfo = [String]()
    var DicGeneDiagnose = NSMutableDictionary()
    var ListGene = [String]()

    //keep specific symptons
    var npbPatient = npb_participants()
    
    //key for searching patient
    var patientID = 0
    
    //keep basic patient info
    var current_patient = patient()
    
    @IBOutlet weak var myTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        fetchData(patientID)
        // 1 for default expand, 0 for collapse
        arrayForBool = ["1","0","0"]
        sectionTitleArray = ["DETAILS for PATIENTS","ENROLMENT INFORMATION","GENETIC DIAGNOSIS"]
        var string1 = sectionTitleArray .objectAtIndex(0) as? String
        sectionContentDict.setValue(self.DicPatientInfo, forKey: string1!)
        string1 = sectionTitleArray .objectAtIndex(1) as? String
        sectionContentDict.setValue(self.DicEnrollInfo, forKey: string1!)
        string1 = sectionTitleArray.objectAtIndex(2) as? String
        sectionContentDict.setValue(self.DicGeneDiagnose, forKey:string1! )
        
        self.myTable.registerNib(UINib(nibName: "ExpandingTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.myTable.registerNib(UINib(nibName: "ExpandingTextTableViewCell", bundle: nil), forCellReuseIdentifier: "textCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchData(id:Int){
        let url = "http://localhost:3000/api/v1/npb_participants/\(id)"
        // Call function to connect server by API with specific url
        Alamofire.request(.GET, url, headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
                if(jsonResponse["error"] != nil){
                    self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
                }else{
                //get user info
                let patient = npb_participants()
                patient.symptom_age_unknown = jsonResponse["symptom_age_unknown"].boolValue
                patient.symptom_age_na = jsonResponse["symptom_age_na"].boolValue
                patient.symptom_age_year = jsonResponse["symptom_age_year"].intValue
                patient.symptom_age_month = jsonResponse["symptom_age_month"].intValue
                
                patient.symptom_enlarged_liver = jsonResponse["symptom_enlarged_liver"].boolValue
                patient.symptom_breathing = jsonResponse["symptom_breathing"].boolValue
                patient.symptom_anaemia = jsonResponse["symptom_anaemia"].boolValue
                patient.symptom_bleeding = jsonResponse["symptom_bleeding"].boolValue
                patient.symptom_growth_delay = jsonResponse["symptom_growth_delay"].boolValue
                patient.symptom_developmental_delay = jsonResponse["symptom_developmental_delay"].boolValue
                patient.symptom_bone_pain = jsonResponse["symptom_bone_pain"].boolValue
                patient.symptom_abdominal_pain = jsonResponse["symptom_abdominal_pain"].boolValue
                patient.symptom_other = jsonResponse["symptom_other"].boolValue
                patient.symptom_other_specify = jsonResponse["symptom_other_specify"].stringValue
                
                patient.age_diagnosed_known = jsonResponse["age_diagnosed_known"].boolValue
                patient.age_diagnosed_year = jsonResponse["age_diagnosed_year"].intValue
                patient.age_diagnosed_month = jsonResponse["age_diagnosed_month"].intValue
                
                patient.family_diagnosed = jsonResponse["family_diagnosed"].intValue
                patient.family_diagnosed_relation = jsonResponse["family_diagnosed_relation"].stringValue
                
                patient.gene_test_performed = jsonResponse["gene_test_performed"].boolValue
                patient.gene_diagnosis = jsonResponse["gene_diagnosis"]["url"].stringValue
                patient.unable_to_contact_specialist = jsonResponse["unable_to_contact_specialist"].boolValue
                
                print(patient.symptom_age_month)
                print(patient.symptom_age_year)
                self.readJson()
                self.configTable(patient)
                }
            }
            self.myTable.reloadData()
        
    }
    
    func readJson(){
        let url = NSBundle.mainBundle().URLForResource("question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        for item in json["npb_participant"].arrayValue{
            self.ques.first_observed = item["first_observed"].stringValue
            self.ques.first_experience = item["first_experience"].stringValue
            self.ques.age_diagnosed = item["age_diagnosed"].stringValue
            self.ques.family = item["family"].stringValue
            self.ques.gene = item["gene"].stringValue
            self.ques.gene_pdf = item["gene_pdf"].stringValue
        }
    }
    
    func configTable(patient: npb_participants){
        
        let id = current_patient.id
        let type = current_patient.enrollable_type
        self.DicPatientInfo["0"] = id
        self.DicPatientInfo["1"] = type
        self.ListPatientInfo = ["PatientID","PatientType"]
        
        //Q1
        if(patient.symptom_age_unknown == true){
            self.DicEnrollInfo["0"] = "Unknown"
        }else if(patient.symptom_age_na == true){
            self.DicEnrollInfo["0"] = "No symptoms"
        }else{
            let time = "\(patient.symptom_age_year) Year, \(patient.symptom_age_month) Month"
            self.DicEnrollInfo["0"] = time
        }
        
        //Q2
        let liver = "Enlarged liver and/or spleen: \n\(patient.symptom_enlarged_liver)"
        let breath = "Breathing or lung problems: \n\(patient.symptom_breathing)"
        let anaemia = "Anaemia: \n\(patient.symptom_anaemia)"
        let bleeding = "Problems with bleeding or excessive bruising: \n\(patient.symptom_bleeding)"
        let growth = "Growth delay: \n\(patient.symptom_growth_delay)"
        let development = "Developmental delay: \n\(patient.symptom_developmental_delay)"
        let bone = "Bone pain :\n\(patient.symptom_bone_pain)"
        let abdominal = "Abdominal pain:\n\(patient.symptom_abdominal_pain)"
        let other = "Other/further information: \n\(patient.symptom_other)"
        let specify = "Please specify :\n\(patient.symptom_other_specify)"
        
        let q2 = "\(liver)\n\(breath)\n\(anaemia)\n\(bleeding)\n\(growth)\n\(development)\n\(bone)\n\(abdominal)\n\(other)\n\(specify)"
        
        self.DicEnrollInfo["1"] = q2
        
        //Q3
        if(patient.age_diagnosed_known == true){
            self.DicEnrollInfo["2"] = "Unknown"
        }else{
            let time = "\(patient.age_diagnosed_year) Year, \(patient.age_diagnosed_month) Month"
            self.DicEnrollInfo["2"] = time
        }
        
        //Q4
        if(patient.family_diagnosed == 0){
            let q4 = "\(patient.family_diagnosed): \(patient.family_diagnosed_relation)"
            self.DicEnrollInfo["3"] = q4
        }else if(patient.family_diagnosed == 1){
            self.DicEnrollInfo["3"] = "No"
        }else{
            self.DicEnrollInfo["3"] = "Unknown"
        }
        self.ListEnrollInfo = [ques.first_observed,ques.first_experience,ques.age_diagnosed,ques.family]
        
        //config List3 Q1
        self.ListGene = [ques.gene_pdf,ques.gene]
        if(patient.gene_diagnosis == ""){
            self.DicGeneDiagnose["0"] = "No PDF"
        }else{
            self.DicGeneDiagnose["0"] = patient.gene_diagnosis
        }
        
        let unable_contact = "Give consent for us to contact your specialist on your behalf using the information you provided previously: \n\(patient.unable_to_contact_specialist)"
        let test_performe = "A genetic test has not been performed: \n\(patient.gene_test_performed)"
        let q5 = "\(unable_contact)\n\(test_performe)"
        self.DicGeneDiagnose["1"] = q5
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitleArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(arrayForBool .objectAtIndex(section).boolValue == true)
        {
            let tps = sectionTitleArray.objectAtIndex(section) as! String
            let count1 = (sectionContentDict.valueForKey(tps)?.allValues) as! NSArray
            return count1.count
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "ABC"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(arrayForBool.objectAtIndex(indexPath.section).boolValue == true){
            if(indexPath.section == 1 && indexPath.row == 1){return 480}
            else if(indexPath.section == 2 && indexPath.row == 1){
                return 250
            }
            return 100
        }

        return 3;
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        headerView.backgroundColor = UIColor(red: 255/255, green: 250/255, blue: 244/255, alpha: 1)
        headerView.tag = section
        
        let headerString = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.frame.size.width-10, height: 30)) as UILabel
        headerString.text = sectionTitleArray.objectAtIndex(section) as? String
        headerString.textColor = UIColor(red: 207/255, green: 165/255, blue: 164/255, alpha: 1)
        headerView .addSubview(headerString)
        
        let headerTapped = UITapGestureRecognizer (target: self, action:#selector(ExpandingViewController.sectionHeaderTapped(_:)))
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView

    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        print("Tapping working")
        print(recognizer.view?.tag)
        
        let indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        if (indexPath.row == 0) {
            
            var collapsed = arrayForBool.objectAtIndex(indexPath.section).boolValue
            collapsed = !collapsed;
            
            arrayForBool .replaceObjectAtIndex(indexPath.section, withObject: collapsed)
            //reload specific section animated
            let range = NSMakeRange(indexPath.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.myTable .reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //        let index = indexPath.section
        if(sectionTitleArray.objectAtIndex(indexPath.section) as! String == "ENROLMENT INFORMATION"){
            let cellIdentifier2 = "textCell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier2) as! ExpandingTextTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.viewQ.text = ListEnrollInfo[Int(sorted[indexPath.row])!]
                cell.viewA.text = String(DicEnrollInfo.valueForKey(sorted[indexPath.row])!)
            }
            return cell
        }
        else{
            let cellIdentifier2 = "textCell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier2) as! ExpandingTextTableViewCell
            
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.viewQ.text = ListEnrollInfo[Int(sorted[indexPath.row])!]
                cell.viewA.text = String(DicGeneDiagnose.valueForKey(sorted[indexPath.row])!)

            }
            return cell
        }
    }
  
}
