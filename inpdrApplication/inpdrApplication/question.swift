//
//  question.swift
//  inpdrApplication
//
//  Created by Janice on 14/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation

class question {
    var baby_sympton = ""
    var relation = ""
    var first_observed = ""
    var first_experience = ""
    var age_diagnosed = ""
    var family = ""
    var gene = ""
    var gene_pdf = ""
}
