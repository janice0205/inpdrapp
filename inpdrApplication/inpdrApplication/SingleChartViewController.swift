//
//  SingleChartViewController.swift
//  inpdrApplication
//
//  Created by Janice on 30/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
class SingleChartViewController: UIViewController,ChartViewDelegate {
    
    var patientData = patient()
    var npc_follows = [npc_followup]()
    var Lists = [Double]()
    var type = ""
    let questionsC = ["Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q11", "Q12","Q13","Q14","Q15"]
    
    let questionsB = ["Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q11", "Q12","Q13","Q14","Q15","Q16", "Q17", "Q18", "Q19", "Q20", "Q21", "Q22","Q23","Q24"]
    @IBOutlet var lineChartView: LineChartView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lineChartView.delegate = self
        
        self.lineChartView.descriptionText = "Tap node for details"
        // 3
        self.lineChartView.descriptionTextColor = UIColor.whiteColor()
        self.lineChartView.gridBackgroundColor = UIColor.darkGrayColor()
        // 4
        self.lineChartView.noDataText = "No data provided"
       
        if(self.type == "NpcParticipant" || self.type == "NpcApplicantParticipant"){
            self.setChartData(self.questionsC, values: self.Lists)

            //                    self.setChart(self.questionsC, values: self.calculate(self.NumList, List: self.Lists))
        }else{
            self.setChartData(self.questionsB, values: self.Lists)
        }
    }

        // Do any additional setup after loading the view.

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setChartData(questions : [String], values: [Double]) {
        
       
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        for i in 0 ..< questions.count {
            yVals1.append(ChartDataEntry(value: values[i], xIndex: i))
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(yVals: yVals1, label: "First Set")
        set1.axisDependency = .Left // Line will correlate with left axis values
        set1.setColor(UIColor.redColor().colorWithAlphaComponent(0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.redColor()) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 6.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.redColor()
        set1.highlightColor = UIColor.whiteColor()
        set1.drawCircleHoleEnabled = true
        
        
        
        //3 - create an array to store our LineChartDataSets
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData = LineChartData(xVals: questions, dataSets: dataSets)
        data.setValueTextColor(UIColor.whiteColor())
        
        //5 - finally set our data
        self.lineChartView.data = data
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
