//
//  followup.swift
//  inpdrApplication
//
//  Created by Janice on 16/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class followup {
    //npc
    var impact_ambulation = ""
    var impact_manipulation = ""
    var speech = ""
    var swallow = ""
    var eye_movement = ""
    var seizures = ""
//    var cognitive = ""
//    var family_disapponited = ""
//    var family_giveup = ""
//    var family_worry = ""
//    var close_family = ""
//    var wider_miss = ""
//    var wider_carer = ""
//    var emergency = ""
//    var appointment = ""
    //npb
    
    var bone_pain = ""
    var abdominal = ""
    var pain_impact_rate = ""
    var breath = ""
    var breath_rate = ""
    var bleed = ""
    var bleed_rate = ""
    var tired = ""
    var fatigue_impact = ""
    var organ_enlarge = ""
    var organ_rate = ""
    var slow_groth = ""
    var slow_rate = ""
    var infection = ""
    var fractures = ""
    
    //share parts
    var cognitive = ""
    var family_disapponited = ""
    var family_giveup = ""
    var family_worry = ""
    var close_family = ""
    var wider_miss = ""
    var wider_carer = ""
    var emergency = ""
    var appointment = ""
}
