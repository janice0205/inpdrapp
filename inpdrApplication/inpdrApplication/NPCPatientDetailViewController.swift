//
//  PatientDetailViewController.swift
//  inpdrApplication
//
//  Created by Janice on 3/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class NPCPatientDetailViewController: UIViewController {

    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!

    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    var npcPatient = npc_participant()
    var current_patient = patient()
    var patient_type = "npc_participants"
    var patient_id = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        maleBtn.enabled = false
        femaleBtn.enabled = false
        myScrollView.contentSize.height = 1000
        fetchData(patient_type, id: patient_id)
        loadData()


        // Do any additional setup after loading the view.
    }
    
    func loadData(){
        self.NameLbl.text = current_patient.patient_first_name + " "+current_patient.patient_last_name
        self.dobLbl.text = current_patient.date_of_birth
        if(current_patient.gender == 0){
            self.maleBtn.enabled = true
            self.maleBtn.selected = true
        }else{
            self.femaleBtn.enabled = true
            self.femaleBtn.selected = true
        }
        
//        self.addressLbl.text = current_patient.addresses_id
    }
    func fetchData(type:String,id:Int){
        let cn = connection()
        let url = "http://localhost:3000/api/v1/"+type+"/"+String(id)
        print(url)
        // Call function to connect server by API with specific url
        cn.connectServer_Get(url) { (jsonResponse) in
            //Parsing JSON file
            print(jsonResponse)
//            for item in jsonResponse.arrayValue{
//                //get user info
//               print(js)
            
//                self.patients.append(current_patient!)
            }
        }
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
