//
//  connection.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class connection{
    var url: String = ""
    func connectServer_Get(url:String,completionHandler: (jsonResponse: JSON) -> ()) {
        
        Alamofire.request(.GET, url)
            .responseJSON { (response) in
                print(response.result.debugDescription)
                let json = JSON(response.result.value!)
                //        print(json) // works fine
                completionHandler(jsonResponse: json)
        }
    }
    
    func connectServer_Delete(url: String, completionHandler: (statu: String) ->()){
        Alamofire.request(.DELETE, url)
            .responseJSON { response in
                let status = response.result.description
                print(status)
                completionHandler(statu: status)
        }
    }
//    
}