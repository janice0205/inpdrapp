//
//  AddFollowWiderViewController.swift
//  inpdrApplication
//
//  Created by Janice on 17/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AddFollowWiderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var myTable: UITableView!
    
    var dictHeader = [String: String]()
    
    var selectedAnswerForRow: [Int:String] = [:]
    
    var dictPicker = [Int: [String]]()
    var startIndex: Int?
    var listQuestion3 = [String]()
    var followques = followup()
    var dictAnswer = [Int : Int]()
    var followable_id : Int?
    var type = ""
    
    var alert = SweetAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myTable.registerNib(UINib(nibName: "AddFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "addFollowCell")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .Plain, target: self, action: #selector(submitFollowup))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return listQuestion3.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = myTable.dequeueReusableCellWithIdentifier("addFollowCell") as! AddFollowTableViewCell
        if(cell.identifier == true){
            cell.answerText.text = selectedAnswerForRow[indexPath.row]
        }
        cell.questionView.text = listQuestion3[indexPath.row]
        cell.pickerDataSource = dictPicker[indexPath.row + startIndex!]!
        dictAnswer[indexPath.row + startIndex!] = cell.pickerValue
        cell.answerText.addTarget(self, action: #selector(AddFollowUpViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingDidEnd)
        cell.answerText.tag = indexPath.row
        cell.identifier = true
        return cell
    }
    
    func textFieldDidChange(sender: UITextField){
        let cell = sender.superview?.superview as! AddFollowTableViewCell
        dictAnswer[sender.tag + startIndex!] = cell.pickerValue
        let  rowIndex = myTable.indexPathForCell(cell)?.row
        selectedAnswerForRow[rowIndex!] = cell.answerValue
    }
    
    
    func submitFollowup(){
        print(dictAnswer)
        if(dictAnswer.values.contains(-1)){
            alert.showAlert("Error", subTitle: "Please fill all questions", style: AlertStyle.Error)
        }else{
            let paramListC = ["impact_ambulation","impact_manipulation","impact_speech","impact_swallowing","impact_eye_movement","impact_seizure","impact_cognitive_impaired","impact_family_disappointed","impact_family_give_up","impact_family_worry_future","impact_family_closer_family","impact_wider_self_miss_career","impact_wider_family_miss_career","impact_wider_emergency","impact_wider_appointment"]
            let paramListB = ["symptom_bone","symptom_abdominal","symptom_pain","symptom_breathlessness_rate","symptom_breathlessness_impact","symptom_bleeding_rate","symptom_bleeding_impact","symptom_fatigue_rate","symptom_fatigue_impact","symptom_enlarge_organ","symptom_enlarge_organ_impact","symptom_slow_growth","symptom_slow_growth_impact","symptom_infection","symptom_fracture","symptom_cognition","impact_family_disappointed","impact_family_give_up","impact_family_worry_future","impact_family_closer","impact_wider_self_miss_career","impact_wider_carer_miss_career","impact_wider_emergency","impact_wider_appointment"]
            var url = ""
            var List = [String]()
            if (type == "NpbParticipant" || type == "NpbApplicantParticipant"){
                url = "http://localhost:3000/api/v1/\(type)/\(Int(followable_id!))/npb_followups"
                List = paramListB
            }else{
                url = "http://localhost:3000/api/v1/\(type)/\(Int(followable_id!))/npc_followups"
                List = paramListC
            }
            SweetAlert().showAlert("Are you sure?", subTitle: "Your followUp will be submitted!", style: AlertStyle.Warning, buttonTitle:"No, cancel plx!", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes, submit it!", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
                if isOtherButton == true {
                    SweetAlert().showAlert("Cancelled!", subTitle: "Cancelled your submission", style: AlertStyle.Error)
                }
                else {
                    var dictParam = [String : AnyObject]()
                    for i in 0..<List.count{
                        if (self.dictAnswer[i] < 0){
                            continue
                        }
                        dictParam[List[i]] = self.dictAnswer[i]
                    }
                    dictParam["followable_id"] = self.followable_id
                    dictParam["followable_type"] = self.type
                    Alamofire.request(.POST, url,headers: self.dictHeader, parameters: dictParam, encoding: .JSON).responseJSON { response in
                        var json = JSON(response.result.value!)
                        print(response.result.isSuccess)
                        if(!response.result.isSuccess && json["error"] != nil){
                            print("alert")
                            SweetAlert().showAlert("Error!", subTitle: json["error"].stringValue, style: AlertStyle.Warning)
                        }else{
                            SweetAlert().showAlert("Successfully submitted!", subTitle: "Your new follow up has been submitted!", style: AlertStyle.Success)
                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("followupVC") as! FollowUpViewController
                            vc.type = self.type
                            vc.enrollable_id = self.followable_id
                            vc.navigationItem.title = "FollowUps"
                            vc.dictHeader = self.dictHeader
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            } }
    }
}
