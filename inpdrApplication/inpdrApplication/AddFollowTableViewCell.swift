//
//  AddFollowTableViewCell.swift
//  inpdrApplication
//
//  Created by Janice on 16/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class AddFollowTableViewCell: UITableViewCell,UIPickerViewDataSource, UIPickerViewDelegate{

    @IBOutlet weak var answerText: UITextField!
    var answerPicker = UIPickerView()
    var pickerValue = -1
    var answerValue = ""
    var identifier = false
    
    @IBOutlet weak var questionView: UITextView!
    
    var pickerDataSource = ["White", "Red", "Green", "Blue"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        answerPicker.delegate = self
        answerPicker.dataSource = self
        answerText.inputView = answerPicker
        
        //Init toolbar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 85/255, green: 180/255, blue: 167/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddFollowTableViewCell.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddFollowTableViewCell.canclePicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        answerText.inputAccessoryView = toolBar
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //Picker View
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row] 
    }
//    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // selected value in Uipickerview in Swift
        answerText.text = pickerDataSource[row]
        answerValue = pickerDataSource[row]
        pickerValue = row
    }

    func donePicker(){
        answerText.resignFirstResponder()
    }
    
    func canclePicker(){
        answerText.text = ""
        answerText.resignFirstResponder()
    }
    
    //modify font size of picker
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView
    {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = pickerDataSource[row]
        // pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    

}
