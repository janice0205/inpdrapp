//
//  EditFollowUpViewController.swift
//  inpdrApplication
//
//  Created by Janice on 20/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class EditFollowUpViewController: UIViewController {

    @IBOutlet weak var myTable: UITableView!
    
    var dictHeader = [String: String]()

    var dictPicker = [Int: [String]]()
    var listQuestion1 = [String]()
    var listQuestion2 = [String]()
    var listQuestion3 = [String]()
    var followques = followup()
    
    var alert = SweetAlert()
    
    var dictAnswer = [Int: Int]()
    var dictAnswer1 = [Int: Int]()

    var type = ""
    var followable_id : Int?
    var id :Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(editFamilyImpact))
        self.myTable.registerNib(UINib(nibName: "EditTableViewCell", bundle: nil), forCellReuseIdentifier: "editCell")
        readJsonQuestions()
        readJsonQuestionsChoices()
        if(type == "NpcParticipant" || type == "NpcApplicantParticipant"){
           fetchDataC(id!, followable_id: followable_id!, followable_type: type)
        }else{
        fetchDataB(id!, followable_id: followable_id!, followable_type: type)
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonQuestionsChoices(){
        let url = NSBundle.mainBundle().URLForResource("follow_question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = ""
        switch type {
        case "NpbParticipant":
            source = "npb_question"
        case "NpbApplicantParticipant":
            source = "npb_question"
            
        case "NpcParticipant":
            source = "npc_question"
        case "NpcApplicantParticipant":
            source = "npc_applicant_participants"
        default:
            source = "npc_question"
        }
        for i in 0..<json[source].count {
            var tmpList = [String]()
            for j in 0..<json[source].arrayValue[i].count{
                tmpList.append(String(json[source][i][j]))
            }
            dictPicker[i] = tmpList
        }
    }
    
    func readJsonQuestions(){
        let url = NSBundle.mainBundle().URLForResource("followup", withExtension: "json")
        var source = ""
        switch type {
        case "NpbParticipant":
            source = "npb_follow"
        case "NpbApplicantParticipant":
            source = "npbA_follow"
            
        case "NpcParticipant":
            source = "npc_follow"
        case "NpcApplicantParticipant":
            source = "npcA_follow"
        default:
            source = "npc_follow"
        }
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        for item in json[source].arrayValue{
            if(type == "NpcParticipant" || type == "NpcApplicantParticipant"){
                listQuestion1.append(item["impact_ambulation"].stringValue)
                listQuestion1.append(item["impact_manipulation"].stringValue)
                listQuestion1.append(item["impact_speech"].stringValue)
                listQuestion1.append(item["impact_swallowing"].stringValue)
                listQuestion1.append(item["impact_eye_movement"].stringValue)
                listQuestion1.append(item["impact_seizure"].stringValue)
                listQuestion1.append(item["impact_cognitive_impaired"].stringValue)
                listQuestion2.append(item["impact_family_disappointed"].stringValue)
                listQuestion2.append(item["impact_family_give_up"].stringValue)
                listQuestion2.append(item["impact_family_worry_future"].stringValue)
                listQuestion2.append(item["impact_family_closer_family"].stringValue)
                listQuestion3.append(item["impact_wider_self_miss_career"].stringValue)
                listQuestion3.append(item["impact_wider_family_miss_career"].stringValue)
                
            }else{
                listQuestion1.append(item["symptom_bone"].stringValue)
                listQuestion1.append(item["symptom_abdominal"].stringValue)
                listQuestion1.append(item["symptom_pain"].stringValue)
                listQuestion1.append(item["symptom_breathlessness_rate"].stringValue)
                listQuestion1.append(item["symptom_breathlessness_impact"].stringValue)
                listQuestion1.append(item["symptom_bleeding_rate"].stringValue)
                listQuestion1.append(item["symptom_bleeding_impact"].stringValue)
                listQuestion1.append(item["symptom_fatigue_rate"].stringValue)
                listQuestion1.append(item["symptom_fatigue_impact"].stringValue)
                listQuestion1.append(item["symptom_enlarge_organ"].stringValue)
                listQuestion1.append(item["symptom_enlarge_organ_impact"].stringValue)
                listQuestion1.append(item["symptom_slow_growth"].stringValue)
                listQuestion1.append(item["symptom_slow_growth_impact"].stringValue)
                listQuestion1.append(item["symptom_infection"].stringValue)
                listQuestion1.append(item["symptom_fracture"].stringValue)
                listQuestion1.append(item["symptom_cognition"].stringValue)
                listQuestion2.append(item["impact_family_disappointed"].stringValue)
                listQuestion2.append(item["impact_family_give_up"].stringValue)
                listQuestion2.append(item["impact_family_worry_future"].stringValue)
                listQuestion2.append(item["impact_family_closer"].stringValue)
                listQuestion3.append(item["impact_wider_self_miss_career"].stringValue)
                listQuestion3.append(item["impact_wider_carer_miss_career"].stringValue)
            }
            listQuestion3.append(item["impact_wider_emergency"].stringValue)
            listQuestion3.append(item["impact_wider_appointment"].stringValue)
        }
        
    }
    
    
    func fetchDataC(id:Int, followable_id: Int, followable_type: String ){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npc_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
                for item in jsonResponse["npc_followups"].arrayValue{
                    //get followup info
                    self.dictAnswer[0] = item["impact_ambulation"].intValue
                    self.dictAnswer[1] = item["impact_manipulation"].intValue
                    self.dictAnswer[2] = item["impact_speech"].intValue
                    self.dictAnswer[3] = item["impact_swallowing"].intValue
                    self.dictAnswer[4] = item["impact_eye_movement"].intValue
                    self.dictAnswer[5] = item["impact_seizure"].intValue
                    self.dictAnswer[6] = item["impact_cognitive_impaired"].intValue
                    self.dictAnswer[7] = item["impact_family_disappointed"].intValue
                    self.dictAnswer[8] = item["impact_family_give_up"].intValue
                    self.dictAnswer[9] = item["impact_family_worry_future"].intValue
                    self.dictAnswer[10] = item["impact_family_closer_family"].intValue
                    self.dictAnswer[11] = item["impact_wider_self_miss_career"].intValue
                    self.dictAnswer[12] = item["impact_wider_family_miss_career"].intValue
                    self.dictAnswer[13] = item["impact_wider_emergency"].intValue
                    self.dictAnswer[14] = item["impact_wider_appointment"].intValue
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myTable.reloadData()
                    }
                }
                self.myTable.reloadData()
        }
    }
    
    func fetchDataB(id:Int, followable_id: Int, followable_type: String ){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npb_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
                for item in jsonResponse["npb_followups"].arrayValue{
                    //get followup info
                    self.dictAnswer[0] = item["symptom_bone"].intValue
                    self.dictAnswer[1] = item["symptom_abdominal"].intValue
                    self.dictAnswer[2] = item["symptom_pain"].intValue
                    self.dictAnswer[3] = item["symptom_breathlessness_rate"].intValue
                    self.dictAnswer[4] = item["symptom_breathlessness_impact"].intValue
                    self.dictAnswer[5] = item["symptom_bleeding_rate"].intValue
                    self.dictAnswer[6] = item["symptom_bleeding_impact"].intValue
                    self.dictAnswer[7] = item["symptom_fatigue_rate"].intValue
                    self.dictAnswer[8] = item["symptom_fatigue_impact"].intValue
                    self.dictAnswer[9] = item["symptom_enlarge_organ"].intValue
                    self.dictAnswer[10] = item["symptom_enlarge_organ_impact"].intValue
                    self.dictAnswer[11] = item["symptom_slow_growth"].intValue
                    self.dictAnswer[12] = item["symptom_slow_growth_impact"].intValue
                    self.dictAnswer[13] = item["symptom_infection"].intValue
                    self.dictAnswer[14] = item["symptom_fracture"].intValue
                    self.dictAnswer[15] = item["symptom_cognition"].intValue
                    self.dictAnswer[16] = item["impact_family_disappointed"].intValue
                    self.dictAnswer[17] = item["impact_family_give_up"].intValue
                    self.dictAnswer[18] = item["impact_family_worry_future"].intValue
                    self.dictAnswer[19] = item["impact_family_closer"].intValue
                    self.dictAnswer[20] = item["impact_wider_self_miss_career"].intValue
                    self.dictAnswer[21] = item["impact_wider_carer_miss_career"].intValue
                    self.dictAnswer[22] = item["impact_wider_emergency"].intValue
                    self.dictAnswer[23] = item["impact_wider_appointment"].intValue
                    dispatch_async(dispatch_get_main_queue()) {
                        self.myTable.reloadData()
                    }
                }
                self.myTable.reloadData()
        }
    }
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(dictAnswer.count > listQuestion1.count){
            return listQuestion1.count
        }else{
            return dictAnswer.count
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = myTable.dequeueReusableCellWithIdentifier("editCell") as! EditTableViewCell
        cell.answerText.text = dictPicker[indexPath.row]![dictAnswer[indexPath.row]!]
//        cell.answerText.text = String(dictAnswer[indexPath.row])
        cell.questionView.text = listQuestion1[indexPath.row]
        cell.pickerDataSource = dictPicker[indexPath.row]!
        cell.answerText.addTarget(self, action: #selector(AddFollowUpViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingDidEnd)
        cell.answerText.tag = indexPath.row
        
        return cell
    }
    
    func textFieldDidChange(sender: UITextField){
        print("touched")
        let cell = sender.superview?.superview as! EditTableViewCell
        dictAnswer[sender.tag] = cell.pickerValue
    }
    

    
    func editFamilyImpact(){
        print(dictAnswer)
        if(dictAnswer.values.contains(-1)){
            alert.showAlert("Error", subTitle: "Please fill all questions", style: AlertStyle.Error)
        }else{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("editFamily") as! EditFollowFamilyViewController
            vc.dictHeader = dictHeader
            vc.dictPicker = self.dictPicker
            vc.startIndex = listQuestion1.count
            vc.listQuestion2 = self.listQuestion2
            vc.listQuestion3 = self.listQuestion3
            vc.followques = followques
            vc.navigationItem.title = "Impact on Family"
            vc.dictAnswer = dictAnswer
            vc.type = type
            vc.id = id
            vc.followable_id = followable_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}
