//
//  npb_followup.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class npb_followup {
    var id  = 0
    var symptom_bone = 0
    var symptom_abdominal = 0
    var symptom_pain = 0
    var symptom_breathlessness_rate = 0
    var symptom_breathlessness_impact = 0
    var symptom_bleeding_rate = 0
    var symptom_bleeding_impact = 0
    var symptom_fatigue_rate = 0
    var symptom_fatigue_impact = 0
    var symptom_enlarge_organ = 0
    var symptom_enlarge_organ_impact = 0
    var symptom_slow_growth = 0
    var symptom_slow_growth_impact = 0
    var symptom_infection = 0
    var symptom_fracture = 0
    var symptom_cognition = 0
    var impact_family_disappointed = 0
    var impact_family_give_up = 0
    var impact_family_worry_future = 0
    var impact_family_closer = 0
    var impact_wider_self_miss_career = 0
    var impact_wider_carer_miss_career = 0
    var impact_wider_emergency = 0
    var impact_wider_appointment = 0
    var created_at = ""
    var updated_at = ""
    var followable_id = 0
    var followable_type = ""
    
    
//    init?(symptom_bone:Int,symptom_abdominal:Int,symptom_pain:Int,symptom_breathlessness_rate:Int,symptom_breathlessness_impact:Int,symptom_bleeding_rate:Int,symptom_bleeding_impact:Int, symptom_fatigue_rate:Int,symptom_fatigue_impact:Int,symptom_enlarge_organ:Int,symptom_enlarge_organ_impact:Int,symptom_slow_growth:Int,symptom_slow_growth_impact:Int,symptom_infection:Int,symptom_fracture:Int,symptom_cognition:Int,impact_family_disappointed:Int,impact_family_give_up:Int,impact_family_worry_future:Int,impact_family_closer:Int,impact_wider_self_miss_career:Int,impact_wider_carer_miss_career:Int,impact_wider_emergency:Int,impact_wider_appointment:Int,followable_id:Int,followable_type:String){
//        self.symptom_bone = symptom_bone
//        self.symptom_abdominal = symptom_abdominal
//        self.symptom_pain = symptom_pain
//        self.symptom_breathlessness_rate = symptom_breathlessness_rate
//        self.symptom_breathlessness_impact = symptom_breathlessness_impact
//        self.symptom_bleeding_rate = symptom_bleeding_rate
//        self.symptom_bleeding_impact = symptom_bleeding_impact
//        self.symptom_fatigue_rate = symptom_fatigue_rate
//        self.symptom_fatigue_impact = symptom_fatigue_impact
//        self.symptom_enlarge_organ = symptom_enlarge_organ
//        self.symptom_enlarge_organ_impact = symptom_enlarge_organ_impact
//        self.symptom_slow_growth = symptom_slow_growth
//        self.symptom_slow_growth_impact = symptom_slow_growth_impact
//        self.symptom_infection = symptom_infection
//        self.symptom_fracture = symptom_fracture
//        self.symptom_cognition = symptom_cognition
//        self.impact_family_disappointed = impact_family_disappointed
//        self.impact_family_give_up = impact_family_give_up
//        self.impact_family_worry_future = impact_family_worry_future
//        self.impact_family_closer = impact_family_closer
//        self.impact_wider_self_miss_career = impact_wider_self_miss_career
//        self.impact_wider_carer_miss_career = impact_wider_carer_miss_career
//        self.impact_wider_emergency = impact_wider_emergency
//        self.impact_wider_appointment = impact_wider_appointment
//        self.followable_id = followable_id
//        self.followable_type = followable_type
//        
//    }
}

