//
//  npc_applicant_participant.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class npc_applicant_participant{

    var id = 0
    var age_diagnosed_known = false
    var age_diagnosed_year = 0
    var age_diagnosed_month = 0
    var family_diagnosed = 0
    var family_diagnosed_relation = ""
    var applicant_relation = 0
    var applicant_relation_specify = ""
    var hospital_name = ""
    var hospital_address = ""
    var hospital_email = ""
    var gene_test_performed = false
    var baby_symptoms = 0
    var symptom_age_year = 0
    var symptom_age_month = 0
    var symptom_age_unknown = false
    var symptom_age_na = false
    var symptom_delayed_milestone = false
    var symptom_ppers = false
    var symptom_coordication = false
    var symptom_eye = false
    var symptom_behavioural = false
    var symptom_seizure = false
    var symptom_psychiatric = false
    var symptom_other = false
    var symptom_other_specify = ""
    var hospital_clinician_name = ""
    var gene_diagnosis = ""
    var unable_to_contact_specialist = false
    var not_finish = false
}