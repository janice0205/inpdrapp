//
//  address.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class address{
    var id:Int
    var line_1 : String
    var line_2 : String
    var town : String
    var county : String
    var post_code : String
    var country : String
    var person_id : Int
    var person_type:String
    
    init?(id:Int,line_1: String, line_2: String, town: String, county: String, post_code:String, country: String, person_id: Int,person_type:String ){
        self.id = id
        self.line_1 = line_1
        self.line_2 = line_2
        self.town = town
        self.county = county
        self.post_code = post_code
        self.country = country
        self.person_id = person_id
        self.person_type = person_type
    }
    
}