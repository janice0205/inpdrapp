//
//  specialist.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class specialist{
    var id:Int
    var name: String
    var address: String
    var email: String
    var patient_id: Int
    var patient_type: String
    var hospital_name: String
    var number: String
    
    init?(id:Int,name:String,address:String,email:String,patient_id:Int,patient_type:String,hospital_name:String,number:String){
        self.id = id
        self.name = name
        self.address = address
        self.email = email
        self.patient_id = patient_id
        self.patient_type = patient_type
        self.hospital_name = hospital_name
        self.number = number
    }
}