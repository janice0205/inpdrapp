//
//  showFollowUpViewController.swift
//  inpdrApplication
//
//  Created by Janice on 16/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class showFollowUpViewController: UIViewController {

    @IBOutlet weak var myTable: UITableView!
    var sectionTitleArray : NSMutableArray = NSMutableArray()
    var sectionContentDict : NSMutableDictionary = NSMutableDictionary()
    var arrayForBool : NSMutableArray = NSMutableArray()
    
    var dictHeader = [String: String]()

    var DicImpactInfo = NSMutableDictionary()
    var ListImpactInfo = [String]()
    var DicFamilyInfo = NSMutableDictionary()
    var ListFamilyInfo = [String]()
    var DicWiderImpact = NSMutableDictionary()
    var ListWiderImpact = [String]()
    
    var followques = followup()
    
    var dictPicker = [Int: [String]]()

    //key for searching patient
    var followable_id = 0
    
    var type = ""
    
    var id = 0
    
//    //keep basic patient info
//    var current_patient = patient()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        readJsonQuestionsChoices()
        fetchData(self.id, followable_id: self.followable_id, followable_type: type)
        // 1 for default expand, 0 for collapse
        arrayForBool = ["1","0","0"]
        sectionTitleArray = ["NP-C Impact Questions","Impact on Family","Wider Impact"]
        var string1 = sectionTitleArray .objectAtIndex(0) as? String
        sectionContentDict.setValue(self.DicImpactInfo, forKey: string1!)
        string1 = sectionTitleArray.objectAtIndex(1) as? String
        sectionContentDict.setValue(self.DicFamilyInfo, forKey:string1! )
        string1 = sectionTitleArray.objectAtIndex(2) as? String
        sectionContentDict.setValue(self.DicWiderImpact, forKey: string1!)
        
        var image = UIImage(named: "line-chart.png")
        
        image = image?.imageWithRenderingMode(UIImageRenderingMode.Automatic)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(showSingleChart))
        self.myTable.registerNib(UINib(nibName: "ExpandingTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.myTable.registerNib(UINib(nibName: "ExpandingTextTableViewCell", bundle: nil), forCellReuseIdentifier: "textCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonQuestionsChoices(){
        let url = NSBundle.mainBundle().URLForResource("follow_question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = "npc_question"
        if (type == "NpcApplicantParticipant"){
            source  = "npc_applicant_participants"
        }
        for i in 0..<json[source].count {
            var tmpList = [String]()
            for j in 0..<json[source].arrayValue[i].count{
                tmpList.append(String(json[source][i][j]))
            }
            dictPicker[i] = tmpList
        }
    }

    
    func fetchData(id:Int, followable_id: Int, followable_type: String ){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npc_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
            let jsonResponse = JSON(response.result.value!)
            for item in jsonResponse["npc_followups"].arrayValue{
                //get followup info
                let id = item["id"].intValue
                let impact_ambulation = item["impact_ambulation"].intValue
                let impact_manipulation = item["impact_manipulation"].intValue
                let impact_speech = item["impact_speech"].intValue
                let impact_swallowing = item["impact_swallowing"].intValue
                let impact_eye_movement = item["impact_eye_movement"].intValue
                let impact_seizure = item["impact_seizure"].intValue
                let impact_cognitive_impaired = item["impact_cognitive_impaired"].intValue
                let impact_family_disappointed = item["impact_family_disappointed"].intValue
                let impact_family_give_up = item["impact_family_give_up"].intValue
                let impact_family_worry_future = item["impact_family_worry_future"].intValue
                let impact_family_closer_family = item["impact_family_closer_family"].intValue
                let impact_wider_self_miss_career = item["impact_wider_self_miss_career"].intValue
                let impact_wider_family_miss_career = item["impact_wider_family_miss_career"].intValue
                let impact_wider_emergency = item["impact_wider_emergency"].intValue
                let impact_wider_appointment = item["impact_wider_appointment"].intValue
                let created_at = item["created_at"].stringValue
                let updated_at = item["updated_at"].stringValue
                let followable_id = item["followable_id"].intValue
                let followable_type = item["followable_type"].stringValue
                
                let current_npc = npc_followup()
                current_npc.id = id
                current_npc.impact_ambulation = impact_ambulation
                current_npc.impact_manipulation = impact_manipulation
                current_npc.impact_speech = impact_speech
                current_npc.impact_swallowing = impact_swallowing
                current_npc.impact_eye_movement = impact_eye_movement
                current_npc.impact_seizure = impact_seizure
                current_npc.impact_cognitive_impaired = impact_cognitive_impaired
                current_npc.impact_family_disappointed = impact_family_disappointed
                current_npc.impact_family_give_up = impact_family_give_up
                current_npc.impact_family_worry_future = impact_family_worry_future
                current_npc.impact_family_closer = impact_family_closer_family
                current_npc.impact_wider_self_miss_career = impact_wider_self_miss_career
                current_npc.impact_wider_family_miss_career = impact_wider_family_miss_career
                current_npc.impact_wider_emergency = impact_wider_emergency
                current_npc.impact_wider_appointment = impact_wider_appointment
                current_npc.created_at = created_at
                current_npc.updated_at = updated_at
                current_npc.followable_id = followable_id
                current_npc.followable_type = followable_type
                
                self.readJson()
                self.configTable(current_npc)
            }
            self.myTable.reloadData()
        }
    }
    
    func readJson(){
        let url = NSBundle.mainBundle().URLForResource("followup", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = "npc_follow"
        if(type == "NpcApplicantParticipant"){
            source = "npcA_follow"
        }
        for item in json[source].arrayValue{
            self.followques.impact_ambulation = item["impact_ambulation"].stringValue
            self.followques.impact_manipulation = item["impact_manipulation"].stringValue
            self.followques.speech = item["impact_speech"].stringValue
            self.followques.swallow = item["impact_swallowing"].stringValue
            self.followques.eye_movement = item["impact_eye_movement"].stringValue
            self.followques.seizures = item["impact_seizure"].stringValue
            self.followques.cognitive = item["impact_cognitive_impaired"].stringValue
            self.followques.family_disapponited = item["impact_family_disappointed"].stringValue
            self.followques.family_giveup = item["impact_family_give_up"].stringValue
            self.followques.family_worry = item["impact_family_worry_future"].stringValue
            self.followques.close_family = item["impact_family_closer_family"].stringValue
            self.followques.wider_miss = item["impact_wider_self_miss_career"].stringValue
            self.followques.wider_carer = item["impact_wider_family_miss_career"].stringValue
            self.followques.emergency = item["impact_wider_emergency"].stringValue
            self.followques.appointment = item["impact_wider_appointment"].stringValue
        }
    }
    func configTable(follow: npc_followup){
        
        //config List1 Q1
        self.DicImpactInfo["0"] = follow.impact_ambulation
        self.DicImpactInfo["1"] = follow.impact_manipulation
        self.DicImpactInfo["2"] = follow.impact_speech
        self.DicImpactInfo["3"] = follow.impact_swallowing
        self.DicImpactInfo["4"] = follow.impact_eye_movement
        self.DicImpactInfo["5"] = follow.impact_seizure
        self.DicImpactInfo["6"] = follow.impact_cognitive_impaired
        
        self.ListImpactInfo = [followques.impact_ambulation,followques.impact_manipulation,followques.speech,followques.swallow,followques.eye_movement,followques.seizures,followques.cognitive]
        
        //config List2 Q1
        self.DicFamilyInfo["0"] = follow.impact_family_disappointed
        self.DicFamilyInfo["1"] = follow.impact_family_give_up
        self.DicFamilyInfo["2"] = follow.impact_family_worry_future
        self.DicFamilyInfo["3"] = follow.impact_family_closer
        
        
        self.ListFamilyInfo = [followques.family_disapponited,followques.family_giveup,followques.family_worry,followques.close_family]
        
        //config List3 Q1
        self.ListWiderImpact = [followques.wider_miss,followques.wider_carer,followques.emergency,followques.appointment]
        //config List3
        self.DicWiderImpact["0"] = follow.impact_wider_self_miss_career
        self.DicWiderImpact["1"] = follow.impact_wider_family_miss_career
        self.DicWiderImpact["2"] = follow.impact_wider_emergency
        self.DicWiderImpact["3"] = follow.impact_wider_appointment

    }

    func showSingleChart(){
        let List = [DicImpactInfo.allValues, DicFamilyInfo.allValues, DicWiderImpact.allValues].reduce([],combine:+)
        var newList = [Double]()
        for i in 0..<List.count{
            newList.append(Double(List[i] as! NSNumber))
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("singleChart") as! SingleChartViewController
        vc.Lists = newList
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitleArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(arrayForBool .objectAtIndex(section).boolValue == true)
        {
            let tps = sectionTitleArray.objectAtIndex(section) as! String
            let count1 = (sectionContentDict.valueForKey(tps)?.allValues) as! NSArray
            return count1.count
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "ABC"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(arrayForBool.objectAtIndex(indexPath.section).boolValue == true){
//            if(indexPath.section == 1 && indexPath.row == 2){return 400}
//            else if(indexPath.section == 2 && indexPath.row == 1){
//                return 250
//            }
            return 120
        }
        
        return 3;
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        headerView.backgroundColor = UIColor(red: 255/255, green: 250/255, blue: 244/255, alpha: 1)
        headerView.tag = section
        
        let headerString = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.frame.size.width-10, height: 30)) as UILabel
        headerString.text = sectionTitleArray.objectAtIndex(section) as? String
        headerString.textColor = UIColor(red: 207/255, green: 165/255, blue: 164/255, alpha: 1)
        headerView .addSubview(headerString)
        
        let headerTapped = UITapGestureRecognizer (target: self, action:#selector(ExpandingViewController.sectionHeaderTapped(_:)))
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView
    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        
        let indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        if (indexPath.row == 0) {
            
            var collapsed = arrayForBool.objectAtIndex(indexPath.section).boolValue
            collapsed = !collapsed;
            
            arrayForBool .replaceObjectAtIndex(indexPath.section, withObject: collapsed)
            //reload specific section animated
            let range = NSMakeRange(indexPath.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.myTable .reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //        let index = indexPath.section
        if(sectionTitleArray.objectAtIndex(indexPath.section) as! String == "NP-C Impact Questions"){
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListImpactInfo[Int(sorted[indexPath.row])!]
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])!]!
                let ind = DicImpactInfo.valueForKey(sorted[indexPath.row])! as! Int

                cell.contentLbl.text = strs[ind]

            }
            return cell
        }
        else if (sectionTitleArray.objectAtIndex(indexPath.section) as! String == "Impact on Family"){
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListFamilyInfo[Int(sorted[indexPath.row])!]
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])! + 7]!
                let ind = DicFamilyInfo.valueForKey(sorted[indexPath.row])! as! Int

                cell.contentLbl.text = strs[ind]

            }
            return cell
            
        }else{
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListWiderImpact[Int(sorted[indexPath.row])!]
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])! + 11]!
                let ind = DicWiderImpact.valueForKey(sorted[indexPath.row])! as! Int
                cell.contentLbl.text = strs[ind]
                
            }
            return cell
        }
    }
}

