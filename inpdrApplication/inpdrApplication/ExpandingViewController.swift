//
//  ExpandingViewController.swift
//  inpdrApplication
//
//  Created by Janice on 7/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class ExpandingViewController: UIViewController {

    var ques = question()
    
    var alert = SweetAlert()

    var sectionTitleArray : NSMutableArray = NSMutableArray()
    var sectionContentDict : NSMutableDictionary = NSMutableDictionary()
    var arrayForBool : NSMutableArray = NSMutableArray()
    
    var dictHeader = [String: String]()

    var DicPatientInfo = NSMutableDictionary()
    var ListPatientInfo = [String]()
    var DicEnrollInfo = NSMutableDictionary()
    var ListEnrollInfo = [String]()
    var DicGeneDiagnose = NSMutableDictionary()
    var ListGene = [String]()

    //keep specific symptons
    var npcPatient = npc_participant()
   
    //key for searching patient
    var patientID = 0
    
    //keep basic patient info
    var current_patient = patient()
    var ListPro = ["YES", "NO", "Unknown"]
    @IBOutlet weak var myTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        fetchData(patientID)
        // 1 for default expand, 0 for collapse
        arrayForBool = ["1","0","0"]
        sectionTitleArray = ["DETAILS for PATIENTS","ENROLMENT INFORMATION","GENETIC DIAGNOSIS"]
        var string1 = sectionTitleArray .objectAtIndex(0) as? String
        sectionContentDict.setValue(self.DicPatientInfo, forKey: string1!)
        string1 = sectionTitleArray .objectAtIndex(1) as? String
        sectionContentDict.setValue(self.DicEnrollInfo, forKey: string1!)
        string1 = sectionTitleArray.objectAtIndex(2) as? String
        sectionContentDict.setValue(self.DicGeneDiagnose, forKey:string1! )
        
        self.myTable.registerNib(UINib(nibName: "ExpandingTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.myTable.registerNib(UINib(nibName: "ExpandingTextTableViewCell", bundle: nil), forCellReuseIdentifier: "textCell")
        
    }
    
    func configTable(patient: npc_participant){
        
        let id = current_patient.id
        let type = current_patient.enrollable_type
        self.DicPatientInfo["0"] = id
        self.DicPatientInfo["1"] = type
        self.ListPatientInfo = ["PatientID","PatientType"]

        
        //config List1 Q1
        self.DicEnrollInfo["0"] = patient.baby_symptoms

        //Q2
        if(patient.symptom_age_unknown == true){
            self.DicEnrollInfo["1"] = "Unknown"
        }else if(patient.symptom_age_na == true){
            self.DicEnrollInfo["1"] = "No symptoms"
        }else{
            let time = "\(patient.symptom_age_year) Year, \(patient.symptom_age_month) Month"
             self.DicEnrollInfo["1"] = time
        }
        
        //Q3
        let milestone = "Delayed milestone development: \n\(patient.symptom_delayed_milestone)"
        let peers = "Problems at school, not keeping up with peers: \n\(patient.symptom_ppers)"
        let movement = "Problems with coordination and movement: \n\(patient.symptom_coordication)"
        let eye = "Problems with eye movements: \n\(patient.symptom_eye)"
        let behavioural = "Behavioural problems: \n\(patient.symptom_behavioural)"
        let seizures = "Seizures: \n\(patient.symptom_seizure)"
        let psychiatric = "Psychiatric issues :\n\(patient.symptom_psychiatric)"
        let other = "Other/further information: \n\(patient.symptom_other)"
        let specify = "Please specify :\n\(patient.symptom_other_specify)"
        
        let q3 = "\(milestone)\n\(peers)\n\(movement)\n\(eye)\n\(behavioural)\n\(seizures)\n\(psychiatric)\n\(other)\n\(specify)"

        self.DicEnrollInfo["2"] = q3
        
        //Q4
        if(patient.age_diagnosed_known == true){
            self.DicEnrollInfo["3"] = "Unknown"
        }else{
            let time = "\(patient.age_diagnosed_year) Year, \(patient.age_diagnosed_month) Month"
            self.DicEnrollInfo["3"] = time
        }
        
        //Q5
        if(patient.family_diagnosed == 0){
            let q5 = "\(patient.family_diagnosed): \(patient.family_diagnosed_relation)"
            self.DicEnrollInfo["4"] = q5
        }else if(patient.family_diagnosed == 1){
            self.DicEnrollInfo["4"] = "No"
        }else{
            self.DicEnrollInfo["4"] = "Unknown"
        }
        self.ListEnrollInfo = [ques.baby_sympton,ques.first_observed,ques.first_experience,ques.age_diagnosed,ques.family]
        
        //config List2 Q1
        self.ListGene = [ques.gene_pdf,ques.gene]
        if(patient.gene_diagnosis == ""){
            self.DicGeneDiagnose["0"] = "No PDF"
        }else{
            self.DicGeneDiagnose["0"] = patient.gene_diagnosis
        }
        
        let unable_contact = "Give consent for us to contact your specialist on your behalf using the information you provided previously: \n\(patient.unable_to_contact_specialist)"
        let test_performe = "A genetic test has not been performed: \n\(patient.gene_test_performed)"
        let q2 = "\(unable_contact)\n\(test_performe)"
        self.DicGeneDiagnose["1"] = q2
    }
    
    func fetchData(id:Int){
        let url = "http://localhost:3000/api/v1/npc_participants/\(id)"
        // Call function to connect server by API with specific url
        Alamofire.request(.GET, url, headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
        let jsonResponse = JSON(response.result.value!)
        if(jsonResponse["error"] != nil){
                    self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
        }else{
        let patient = npc_participant()
        patient.baby_symptoms = jsonResponse["baby_symptoms"].intValue
        patient.symptom_age_unknown = jsonResponse["symptom_age_unknown"].boolValue
        patient.symptom_age_na = jsonResponse["symptom_age_na"].boolValue
        patient.symptom_age_year = jsonResponse["symptom_age_year"].intValue
        patient.symptom_age_month = jsonResponse["symptom_age_month"].intValue

        patient.symptom_delayed_milestone = jsonResponse["symptom_delayed_milestone"].boolValue
        patient.symptom_ppers = jsonResponse["symptom_ppers"].boolValue
        patient.symptom_coordication = jsonResponse["symptom_coordication"].boolValue
        patient.symptom_eye = jsonResponse["symptom_eye"].boolValue
        patient.symptom_behavioural = jsonResponse["symptom_behavioural"].boolValue
        patient.symptom_seizure = jsonResponse["symptom_seizure"].boolValue
        patient.symptom_psychiatric = jsonResponse["symptom_psychiatric"].boolValue
        patient.symptom_other = jsonResponse["symptom_other"].boolValue
        patient.symptom_other_specify = jsonResponse["symptom_other_specify"].stringValue

        patient.age_diagnosed_known = jsonResponse["age_diagnosed_known"].boolValue
        patient.age_diagnosed_year = jsonResponse["age_diagnosed_year"].intValue
        patient.age_diagnosed_month = jsonResponse["age_diagnosed_month"].intValue

        patient.family_diagnosed = jsonResponse["family_diagnosed"].intValue
        patient.family_diagnosed_relation = jsonResponse["family_diagnosed_relation"].stringValue
        patient.gene_test_performed = jsonResponse["gene_test_performed"].boolValue
        patient.gene_diagnosis = jsonResponse["gene_diagnosis"]["url"].stringValue
        patient.unable_to_contact_specialist = jsonResponse["unable_to_contact_specialist"].boolValue
            
        
        self.readJson()
        self.configTable(patient)
                }
        }
             self.myTable.reloadData()
    }
    
    func readJson(){
        let url = NSBundle.mainBundle().URLForResource("question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        for item in json["npc_participant"].arrayValue{
            self.ques.baby_sympton = item["baby_sympton"].stringValue
            self.ques.first_observed = item["first_observed"].stringValue
            self.ques.first_experience = item["first_experience"].stringValue
            self.ques.age_diagnosed = item["age_diagnosed"].stringValue
            self.ques.family = item["family"].stringValue
            self.ques.gene = item["gene"].stringValue
            self.ques.gene_pdf = item["gene_pdf"].stringValue
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitleArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(arrayForBool .objectAtIndex(section).boolValue == true)
        {
            let tps = sectionTitleArray.objectAtIndex(section) as! String
            let count1 = (sectionContentDict.valueForKey(tps)?.allValues) as! NSArray
            return count1.count
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "ABC"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(arrayForBool.objectAtIndex(indexPath.section).boolValue == true){
            if(indexPath.section == 1 && indexPath.row == 2){return 400}
             else if(indexPath.section == 2 && indexPath.row == 1){
                return 250
            }
            return 120
        }
        
        return 3;
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        headerView.backgroundColor = UIColor(red: 255/255, green: 250/255, blue: 244/255, alpha: 1)
        headerView.tag = section
        
        let headerString = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.frame.size.width-10, height: 30)) as UILabel
        headerString.text = sectionTitleArray.objectAtIndex(section) as? String
        headerString.textColor = UIColor(red: 207/255, green: 165/255, blue: 164/255, alpha: 1)
        headerView .addSubview(headerString)
        
        let headerTapped = UITapGestureRecognizer (target: self, action:#selector(ExpandingViewController.sectionHeaderTapped(_:)))
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView
    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        print("Tapping working")
        print(recognizer.view?.tag)
        
        let indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        if (indexPath.row == 0) {
            
            var collapsed = arrayForBool.objectAtIndex(indexPath.section).boolValue
            collapsed = !collapsed;
            
            arrayForBool .replaceObjectAtIndex(indexPath.section, withObject: collapsed)
            //reload specific section animated
            let range = NSMakeRange(indexPath.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.myTable .reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //        let index = indexPath.section
        if(sectionTitleArray.objectAtIndex(indexPath.section) as! String == "ENROLMENT INFORMATION"){
            let cellIdentifier2 = "textCell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier2) as! ExpandingTextTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                if(indexPath.row == 0){
                    let value = DicEnrollInfo.valueForKey(sorted[indexPath.row])! as! Int
                    cell.viewQ.text = ListEnrollInfo[Int(sorted[indexPath.row])!]
                    cell.viewA.text = ListPro[value]
                    //                    cell.viewA.text = String(DicEnrollInfo.valueForKey(sorted[indexPath.row])!)
                }else{
                
                cell.viewQ.text = ListEnrollInfo[Int(sorted[indexPath.row])!]
                cell.viewA.text = String(DicEnrollInfo.valueForKey(sorted[indexPath.row])!)
                }
            }
            return cell
        } else if (sectionTitleArray.objectAtIndex(indexPath.section) as! String == "DETAILS for PATIENTS"){
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListPatientInfo[Int(sorted[indexPath.row])!]
                cell.contentLbl?.text = String(DicPatientInfo.valueForKey(sorted[indexPath.row])!)
            }
            return cell
            
        } else{
            let cellIdentifier2 = "textCell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier2) as! ExpandingTextTableViewCell
            
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.viewQ.text = ListGene[Int(sorted[indexPath.row])!]
                cell.viewA.text = String(DicGeneDiagnose.valueForKey(sorted[indexPath.row])!)
                
            }
            return cell
        }
    }
}


