//
//  ChartsViewController.swift
//  inpdrApplication
//
//  Created by Janice on 23/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class ChartsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var patients = [patient]()
    var dictHeader = [String: String]()
    
    @IBOutlet weak var myTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarController = self.tabBarController as! MyTabBarController
        patients = tabBarController.patients
        dictHeader = tabBarController.dictHeader
        self.myTable.reloadData()
        myTable.delegate = self
        myTable.dataSource = self
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 85/255, green: 180/255, blue: 167/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return patients.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         self.performSegueWithIdentifier("showChart", sender: indexPath);
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "showChart") {
            let vc = segue.destinationViewController as? ShowChartViewController
            let row = myTable.indexPathForSelectedRow?.row
            let patientData = patients[row!]
            vc!.patientData = patientData
            vc?.dictHeader = dictHeader
            vc?.type = patientData.enrollable_type
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let current_patient = patients[indexPath.row]
        
        var cell = myTable.dequeueReusableCellWithIdentifier("chartCell") as! ChartTableViewCell
        if(cell.identifier == true){
            cell = myTable.dequeueReusableCellWithIdentifier("chartCell") as! ChartTableViewCell
        }
        var unique_id = current_patient.unique_id
        let type = current_patient.enrollable_type
        switch type {
        case "NpbParticipant":
            unique_id = "NPB-"+unique_id
        case "NpbApplicantParticipant":
            unique_id = "NPB-"+unique_id
        case "NpcParticipant":
            unique_id = "NPC-"+unique_id
        case "NpcApplicantParticipant":
            unique_id = "NPC-"+unique_id
        default:
            unique_id = "unknown-"+unique_id
        }
        cell.patientLbl.text = unique_id
        cell.identifier = true
        
        return cell
    }
    

}
