//
//  ViewController.swift
//  inpdrApplication
//
//  Created by Janice on 21/04/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let cn = connection()
        // Call function to connect server by API with specific url
        cn.connectServer_Get("http://localhost:3000/api/v1/parti_users/2") { (jsonResponse) in
           // print(jsonResponse)
            //Parsing JSON file
            for item in jsonResponse["parti_users"].arrayValue{
                print(item["participant_type"].stringValue)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
//    {
//        let current_patient = patients[indexPath.row]
//        var cell = myTable.dequeueReusableCellWithIdentifier("patientCell") as! PatientTableViewCell
//        if(cell.identifier == true){
//            cell = myTable.dequeueReusableCellWithIdentifier("patientCell") as! PatientTableViewCell
//        }
//        cell.followupBtn.addTarget(self, action: #selector(showFollowup), forControlEvents: UIControlEvents.TouchUpInside)
//        let type = current_patient.enrollable_type
//        if (current_patient.verified == false){
//            cell.followupBtn.enabled = false
//        }
//    }
    
}

