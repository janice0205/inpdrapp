//
//  ExpandingTableViewCell.swift
//  inpdrApplication
//
//  Created by Janice on 8/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class ExpandingTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var fieldLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
