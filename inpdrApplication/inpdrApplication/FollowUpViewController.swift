//
//  FollowUpViewController.swift
//  inpdrApplication
//
//  Created by Janice on 15/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class FollowUpViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var alert = SweetAlert()
    
    @IBOutlet weak var myTable: UITableView!
    var npc_follows = [npc_followup]()
    var npb_follows = [npb_followup]()
    
    var dictHeader = [String: String]()
    
    var enrollable_id: Int?
    var type = ""
    var followable_id: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .Plain, target: self, action: #selector(addFollowUp))
        if(type == "NpbParticipant" || type == "NpbApplicantParticipant"){
            fetchDataB(self.enrollable_id!, followable_type: self.type)
        }else{
            fetchDataC(self.enrollable_id!, followable_type: self.type)

        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchDataB(followable_id: Int, followable_type: String){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npb_followups/",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
                if(jsonResponse["error"] != nil){
                    self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
                }
                else{
                    for item in jsonResponse["npb_followups"].arrayValue{
                        //get user info
                        let id = item["id"].intValue
                        let symptom_bone = item["symptom_bone"].intValue
                        let symptom_abdominal = item["symptom_abdominal"].intValue
                        let symptom_pain = item["symptom_pain"].intValue
                        let symptom_breathlessness_rate = item["symptom_breathlessness_rate"].intValue
                        let symptom_breathlessness_impact = item["symptom_breathlessness_impact"].intValue
                        let symptom_bleeding_rate = item["symptom_bleeding_rate"].intValue
                        let symptom_bleeding_impact = item["symptom_bleeding_impact"].intValue
                        let symptom_fatigue_rate = item["symptom_fatigue_rate"].intValue
                        let symptom_fatigue_impact = item["symptom_fatigue_impact"].intValue
                        let symptom_enlarge_organ = item["symptom_enlarge_organ"].intValue
                        let symptom_enlarge_organ_impact = item["symptom_enlarge_organ_impact"].intValue
                        let symptom_slow_growth = item["symptom_slow_growth"].intValue
                        let symptom_slow_growth_impact = item["symptom_slow_growth_impact"].intValue
                        let symptom_infection = item["symptom_infection"].intValue
                        let symptom_fracture = item["symptom_fracture"].intValue
                        let symptom_cognition = item["symptom_cognition"].intValue
                        let impact_family_disappointed = item["impact_family_disappointed"].intValue
                        let impact_family_give_up = item["impact_family_give_up"].intValue
                        let impact_family_worry_future = item["impact_family_worry_future"].intValue
                        let impact_family_closer_family = item["impact_family_closer_family"].intValue
                        let impact_wider_self_miss_career = item["impact_wider_self_miss_career"].intValue
                        let impact_wider_carer_miss_career = item["impact_wider_carer_miss_career"].intValue
                        let impact_wider_emergency = item["impact_wider_emergency"].intValue
                        let impact_wider_appointment = item["impact_wider_appointment"].intValue
                        let created_at = item["created_at"].stringValue
                        let updated_at = item["updated_at"].stringValue
                        let followable_id = item["followable_id"].intValue
                        let followable_type = item["followable_type"].stringValue
                        
                        let current_npb = npb_followup()
                        current_npb.id = id
                        current_npb.symptom_bone = symptom_bone
                        current_npb.symptom_abdominal = symptom_abdominal
                        current_npb.symptom_pain = symptom_pain
                        current_npb.symptom_breathlessness_rate = symptom_breathlessness_rate
                        current_npb.symptom_breathlessness_impact = symptom_breathlessness_impact
                        current_npb.symptom_bleeding_rate = symptom_bleeding_rate
                        current_npb.symptom_bleeding_impact = symptom_bleeding_impact
                        current_npb.symptom_fatigue_rate = symptom_fatigue_rate
                        current_npb.symptom_fatigue_impact = symptom_fatigue_impact
                        current_npb.symptom_enlarge_organ = symptom_enlarge_organ
                        current_npb.symptom_enlarge_organ_impact = symptom_enlarge_organ_impact
                        current_npb.symptom_slow_growth = symptom_slow_growth
                        current_npb.symptom_slow_growth_impact = symptom_slow_growth_impact
                        current_npb.symptom_infection = symptom_infection
                        current_npb.symptom_fracture = symptom_fracture
                        current_npb.symptom_cognition = symptom_cognition
                        current_npb.impact_family_disappointed = impact_family_disappointed
                        current_npb.impact_family_give_up = impact_family_give_up
                        current_npb.impact_family_worry_future = impact_family_worry_future
                        current_npb.impact_family_closer = impact_family_closer_family
                        current_npb.impact_wider_self_miss_career = impact_wider_self_miss_career
                        current_npb.impact_wider_carer_miss_career = impact_wider_carer_miss_career
                        current_npb.impact_wider_emergency = impact_wider_emergency
                        current_npb.impact_wider_appointment = impact_wider_appointment
                        current_npb.created_at = created_at
                        current_npb.updated_at = updated_at
                        current_npb.followable_id = followable_id
                        current_npb.followable_type = followable_type
                        
                        self.npb_follows.append(current_npb)
                    }
                    self.myTable.reloadData()
                }
        }
    }
    
    func fetchDataC(followable_id: Int, followable_type: String){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npc_followups/",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
                if(jsonResponse["error"] != nil){
                    self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
                }
                else{
                    for item in jsonResponse["npc_followups"].arrayValue{
                        //get user info
                        let id = item["id"].intValue
                        let impact_ambulation = item["impact_ambulation"].intValue
                        let impact_manipulation = item["impact_manipulation"].intValue
                        let impact_speech = item["impact_speech"].intValue
                        let impact_swallowing = item["impact_swallowing"].intValue
                        let impact_eye_movement = item["impact_eye_movement"].intValue
                        let impact_seizure = item["impact_seizure"].intValue
                        let impact_cognitive_impaired = item["impact_cognitive_impaired"].intValue
                        let impact_family_disappointed = item["impact_family_disappointed"].intValue
                        let impact_family_give_up = item["impact_family_give_up"].intValue
                        let impact_family_worry_future = item["impact_family_worry_future"].intValue
                        let impact_family_closer_family = item["impact_family_closer_family"].intValue
                        let impact_wider_self_miss_career = item["impact_wider_self_miss_career"].intValue
                        let impact_wider_family_miss_career = item["impact_wider_family_miss_career"].intValue
                        let impact_wider_emergency = item["impact_wider_emergency"].intValue
                        let impact_wider_appointment = item["impact_wider_appointment"].intValue
                        let created_at = item["created_at"].stringValue
                        let updated_at = item["updated_at"].stringValue
                        let followable_id = item["followable_id"].intValue
                        let followable_type = item["followable_type"].stringValue
                        
                        let current_npc = npc_followup()
                        current_npc.id = id
                        current_npc.impact_ambulation = impact_ambulation
                        current_npc.impact_manipulation = impact_manipulation
                        current_npc.impact_speech = impact_speech
                        current_npc.impact_swallowing = impact_swallowing
                        current_npc.impact_eye_movement = impact_eye_movement
                        current_npc.impact_seizure = impact_seizure
                        current_npc.impact_cognitive_impaired = impact_cognitive_impaired
                        current_npc.impact_family_disappointed = impact_family_disappointed
                        current_npc.impact_family_give_up = impact_family_give_up
                        current_npc.impact_family_worry_future = impact_family_worry_future
                        current_npc.impact_family_closer = impact_family_closer_family
                        current_npc.impact_wider_self_miss_career = impact_wider_self_miss_career
                        current_npc.impact_wider_family_miss_career = impact_wider_family_miss_career
                        current_npc.impact_wider_emergency = impact_wider_emergency
                        current_npc.impact_wider_appointment = impact_wider_appointment
                        current_npc.created_at = created_at
                        current_npc.updated_at = updated_at
                        current_npc.followable_id = followable_id
                        current_npc.followable_type = followable_type
                        
                        self.npc_follows.append(current_npc)
                    }
                    self.myTable.reloadData()
                }
        }
    }
    
    //Add followups
    func addFollowUp(){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("addFollow") as! AddFollowUpViewController
        vc.type = type
        vc.followable_id = self.enrollable_id
        vc.dictHeader = dictHeader
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(type == "NpbParticipant" || type == "NpbApplicantParticipant"){
            return self.npb_follows.count
        }
        else {return npc_follows.count}
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        print(type)
        if(type == "NpbParticipant" || type == "NpbApplicantParticipant"){
            let current_b = npb_follows[indexPath.row]
            let cell = myTable.dequeueReusableCellWithIdentifier("followCell",forIndexPath: indexPath) as! FollowTableViewCell
            cell.created_at.text = convertToDate(current_b.created_at)
            cell.showBtn.addTarget(self, action: #selector(shownpb), forControlEvents: UIControlEvents.TouchUpInside)
            cell.showBtn.tag = indexPath.row
            cell.editBtn.addTarget(self, action: #selector(editnpb), forControlEvents: UIControlEvents.TouchUpInside)
            cell.editBtn.tag = indexPath.row
            cell.destroyBtn.addTarget(self, action: #selector(deletenpb), forControlEvents: UIControlEvents.TouchUpInside)
            cell.destroyBtn.tag = indexPath.row
            return cell
        }else {
            let current_c = npc_follows[indexPath.row]
            let cell = myTable.dequeueReusableCellWithIdentifier("followCell",forIndexPath:indexPath) as! FollowTableViewCell
            cell.created_at.text = convertToDate(current_c.created_at)
            cell.showBtn.addTarget(self, action: #selector(show), forControlEvents: UIControlEvents.TouchUpInside)
            cell.showBtn.tag = indexPath.row
            cell.editBtn.addTarget(self, action: #selector(editnpc), forControlEvents: UIControlEvents.TouchUpInside)
            cell.editBtn.tag = indexPath.row
            cell.destroyBtn.addTarget(self, action: #selector(deletenpc), forControlEvents: UIControlEvents.TouchUpInside)
            cell.destroyBtn.tag = indexPath.row
            return cell
        }
        
    }
    func show(sender: UIButton){
        let type = npc_follows[sender.tag].followable_type
        let followable_id = npc_follows[sender.tag].followable_id
        let id = npc_follows[sender.tag].id
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("showFollow") as! showFollowUpViewController
        vc.dictHeader = dictHeader
        vc.followable_id = followable_id
        vc.id = id
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func shownpb(sender: UIButton){
        let type = npb_follows[sender.tag].followable_type
        let followable_id = npb_follows[sender.tag].followable_id
        let id = npb_follows[sender.tag].id
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("showNpb") as! showFollowUpNpbViewController
        vc.dictHeader = dictHeader
        vc.followable_id = followable_id
        vc.id = id
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func editnpc(sender: UIButton){
        let type = npc_follows[sender.tag].followable_type
        let followable_id = npc_follows[sender.tag].followable_id
        let id = npc_follows[sender.tag].id
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("editNpc") as! EditFollowUpViewController
        vc.dictHeader = dictHeader
        vc.followable_id = followable_id
        vc.id = id
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func editnpb(sender: UIButton){
        let type = npb_follows[sender.tag].followable_type
        let followable_id = npb_follows[sender.tag].followable_id
        let id = npb_follows[sender.tag].id
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("editNpc") as! EditFollowUpViewController
        vc.dictHeader = dictHeader
        vc.followable_id = followable_id
        vc.id = id
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deletenpc(sender: UIButton){
        SweetAlert().showAlert("Are you sure?", subTitle: "Your followUp will permanently delete!", style: AlertStyle.Warning, buttonTitle:"No, cancel plx!", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes, delete it!", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                SweetAlert().showAlert("Cancelled!", subTitle: "Your imaginary file is safe", style: AlertStyle.Error)
            }
            else {
                let followable_id = self.npc_follows[sender.tag].followable_id
                let id = self.npc_follows[sender.tag].id
                Alamofire.request(.DELETE, "http://localhost:3000/api/v1/\(self.type)/\(followable_id)/npc_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
                    .responseJSON { response in
                        let status = response.result.description
                        print(status)
                        if(status == "SUCCESS"){
                    SweetAlert().showAlert("Deleted!", subTitle: "Your imaginary file has been deleted!", style: AlertStyle.Success)
                    self.myTable.reloadData()
                }else{
                    SweetAlert().showAlert("Error!", subTitle: "Error happens", style: AlertStyle.Warning)
                }
            }
            }
        }
    }
    
    
    func deletenpb(sender: UIButton){
        SweetAlert().showAlert("Are you sure?", subTitle: "Your followUp will permanently delete!", style: AlertStyle.Warning, buttonTitle:"No, cancel plx!", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes, delete it!", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                SweetAlert().showAlert("Cancelled!", subTitle: "Your imaginary file is safe", style: AlertStyle.Error)
            }
            else {
                let followable_id = self.npb_follows[sender.tag].followable_id
                let id = self.npb_follows[sender.tag].id
                Alamofire.request(.DELETE, "http://localhost:3000/api/v1/\(self.type)/\(followable_id)/npb_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
                    .responseJSON { response in
                        let status = response.result.description
                        print(status)
                        if(status == "SUCCESS"){
                            SweetAlert().showAlert("Deleted!", subTitle: "Your imaginary file has been deleted!", style: AlertStyle.Success)
                            self.myTable.reloadData()
                        }else{
                            SweetAlert().showAlert("Error!", subTitle: "Error happens", style: AlertStyle.Warning)
                        }
                }
            }
        }
    }

    func convertToDate(str : String) -> String{
        let strDate = str // "2015-10-06T15:42:34Z"
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter2.timeStyle = .MediumStyle
        dateFormatter2.stringFromDate(dateFormatter.dateFromString(strDate)!)
        let str = dateFormatter2.stringFromDate(dateFormatter.dateFromString(strDate)!)
        return str
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
