//
//  PatientTableViewCell.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class PatientTableViewCell: UITableViewCell {

    @IBOutlet weak var followupBtn: UIButton!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var titleType: UILabel!
    @IBOutlet weak var titleID: UILabel!
    @IBOutlet weak var patientTypelbl: UILabel!
    @IBOutlet weak var patientIDlbl: UILabel!
    
    var identifier = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//    }

    
}

