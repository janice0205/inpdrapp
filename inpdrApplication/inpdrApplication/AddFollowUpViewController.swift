//
//  AddFollowUpViewController.swift
//  inpdrApplication
//
//  Created by Janice on 16/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddFollowUpViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var myTable: UITableView!
    
    var dictHeader = [String: String]()

    var selectedAnswerForRow: [Int:String] = [:]
    var dictPicker = [Int: [String]]()
    var listQuestion1 = [String]()
    var listQuestion2 = [String]()
    var listQuestion3 = [String]()
    var followques = followup()
    
    var alert = SweetAlert()
    
    var dictAnswer = [Int: Int]()
    var type = ""
    var followable_id : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        if(type == "NpcParticipant" || type == "NpcApplicantParticipant"){
            self.navigationItem.title = "NP-C Impact Questions"
        }else{
            self.navigationItem.title = "ASMD NP-B Impact Questions"
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(addFamilyImpact))
        readJsonQuestions()
        readJsonQuestionsChoices()
        self.myTable.registerNib(UINib(nibName: "AddFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "addFollowCell")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonQuestionsChoices(){
        let url = NSBundle.mainBundle().URLForResource("follow_question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = ""
        switch type {
            
        case "NpbParticipant":
            source = "npb_question"
        case "NpbApplicantParticipant":
            source = "npb_question"
            
        case "NpcParticipant":
            source = "npc_question"
        case "NpcApplicantParticipant":
            source = "npc_applicant_participants"
        default:
            source = "npc_question"
        }
        print(source)
        for i in 0..<json[source].count {
            var tmpList = [String]()
            for j in 0..<json[source].arrayValue[i].count{
                tmpList.append(String(json[source][i][j]))
            }
            dictPicker[i] = tmpList
        }
    }
    
    func readJsonQuestions(){
        let url = NSBundle.mainBundle().URLForResource("followup", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = ""
        switch type {
            
        case "NpbParticipant":
            source = "npb_follow"
        case "NpbApplicantParticipant":
            source = "npbA_follow"
            
        case "NpcParticipant":
            source = "npc_follow"
        case "NpcApplicantParticipant":
            source = "npcA_follow"
        default:
            source = "npc_follow"
        }
        for item in json[source].arrayValue{
            if(type == "NpcParticipant" || type == "NpcApplicantParticipant"){
                listQuestion1.append(item["impact_ambulation"].stringValue)
                listQuestion1.append(item["impact_manipulation"].stringValue)
                listQuestion1.append(item["impact_speech"].stringValue)
                listQuestion1.append(item["impact_swallowing"].stringValue)
                listQuestion1.append(item["impact_eye_movement"].stringValue)
                listQuestion1.append(item["impact_seizure"].stringValue)
                listQuestion1.append(item["impact_cognitive_impaired"].stringValue)
                listQuestion2.append(item["impact_family_disappointed"].stringValue)
                listQuestion2.append(item["impact_family_give_up"].stringValue)
                listQuestion2.append(item["impact_family_worry_future"].stringValue)
                listQuestion2.append(item["impact_family_closer_family"].stringValue)
                listQuestion3.append(item["impact_wider_self_miss_career"].stringValue)
                listQuestion3.append(item["impact_wider_family_miss_career"].stringValue)

            }else{
                listQuestion1.append(item["symptom_bone"].stringValue)
                listQuestion1.append(item["symptom_abdominal"].stringValue)
                listQuestion1.append(item["symptom_pain"].stringValue)
                listQuestion1.append(item["symptom_breathlessness_rate"].stringValue)
                listQuestion1.append(item["symptom_breathlessness_impact"].stringValue)
                listQuestion1.append(item["symptom_bleeding_rate"].stringValue)
                listQuestion1.append(item["symptom_bleeding_impact"].stringValue)
                listQuestion1.append(item["symptom_fatigue_rate"].stringValue)
                listQuestion1.append(item["symptom_fatigue_impact"].stringValue)
                listQuestion1.append(item["symptom_enlarge_organ"].stringValue)
                listQuestion1.append(item["symptom_enlarge_organ_impact"].stringValue)
                listQuestion1.append(item["symptom_slow_growth"].stringValue)
                listQuestion1.append(item["symptom_slow_growth_impact"].stringValue)
                listQuestion1.append(item["symptom_infection"].stringValue)
                listQuestion1.append(item["symptom_fracture"].stringValue)
                listQuestion1.append(item["symptom_cognition"].stringValue)
                listQuestion2.append(item["impact_family_disappointed"].stringValue)
                listQuestion2.append(item["impact_family_give_up"].stringValue)
                listQuestion2.append(item["impact_family_worry_future"].stringValue)
                listQuestion2.append(item["impact_family_closer"].stringValue)
                listQuestion3.append(item["impact_wider_self_miss_career"].stringValue)
                listQuestion3.append(item["impact_wider_carer_miss_career"].stringValue)
            }

            listQuestion3.append(item["impact_wider_emergency"].stringValue)
            listQuestion3.append(item["impact_wider_appointment"].stringValue)
        }
        
    }
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return listQuestion1.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = myTable.dequeueReusableCellWithIdentifier("addFollowCell") as! AddFollowTableViewCell
        if(cell.identifier == true){
            cell.answerText.text = selectedAnswerForRow[indexPath.row]
        }
        cell.questionView.text = listQuestion1[indexPath.row]
        cell.pickerDataSource = dictPicker[indexPath.row]!
        dictAnswer[indexPath.row] = cell.pickerValue
        cell.answerText.addTarget(self, action: #selector(AddFollowUpViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingDidEnd)
        cell.answerText.tag = indexPath.row
        cell.identifier = true
        
      return cell
    }

    func textFieldDidChange(sender: UITextField){
        let cell = sender.superview?.superview as! AddFollowTableViewCell
        dictAnswer[sender.tag] = cell.pickerValue
        let  rowIndex = myTable.indexPathForCell(cell)?.row
        selectedAnswerForRow[rowIndex!] = cell.answerValue
    }

    func addFamilyImpact(){
        print(dictAnswer)
        if(dictAnswer.values.contains(-1)){
            alert.showAlert("Error", subTitle: "Please fill all questions", style: AlertStyle.Error)
        }else{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("addFamily") as! AddFollowFamilyViewController
            vc.dictHeader = dictHeader
            vc.dictPicker = self.dictPicker
            vc.startIndex = listQuestion1.count
            vc.listQuestion2 = self.listQuestion2
            vc.listQuestion3 = self.listQuestion3
            vc.followques = followques
            vc.navigationItem.title = "Impact on Family"
            vc.dictAnswer = dictAnswer
            vc.type = type
            vc.followable_id = followable_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

    

