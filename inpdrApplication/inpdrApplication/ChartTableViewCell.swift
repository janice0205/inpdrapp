//
//  ChartTableViewCell.swift
//  inpdrApplication
//
//  Created by Janice on 23/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class ChartTableViewCell: UITableViewCell {

    @IBOutlet weak var patientLbl: UILabel!
    var identifier = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
