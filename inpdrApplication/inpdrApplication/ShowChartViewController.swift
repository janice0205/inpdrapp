//
//  testChartViewController.swift
//  inpdrApplication
//
//  Created by Janice on 24/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON

class ShowChartViewController: UIViewController,ChartViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var chartView: LineChartView!

    @IBOutlet weak var date: UITextField!
    
    var datePicker = UIPickerView()

    var patientData = patient()
    var npc_follows = [npc_followup]()
    var dictHeader = [String: String]()
    var Lists = [[Double]]()
    var DateList = [String]()
    var NumList = [7.0,5.0,7.0,5.0,4.0,4.0,4.0,4.0,5.0,7.0,4.0,4.0,4.0,4.0,4.0,5.0]
    var alert = SweetAlert()
    var type = ""
    
    var selectTag: Int?
    
    let questionsC = ["Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q11", "Q12","Q13","Q14","Q15"]
    
    let questionsB = ["Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8", "Q9", "Q10", "Q11", "Q12","Q13","Q14","Q15","Q16", "Q17", "Q18", "Q19", "Q20", "Q21", "Q22","Q23"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chartView.delegate = self
        // Do any additional setup after loading the view.
        
        loadFollowUpData(patientData.enrollable_id, followable_type: patientData.enrollable_type,completionHandler: { (success) -> Void in
            // When download completes,control flow goes here.
            if success {
                // download success
                if(self.type == "NpcParticipant" || self.type == "NpcApplicantParticipant"){
                    self.setChart(self.questionsC, values: self.Lists)
                }else{
                    self.setChart(self.questionsB, values: self.Lists)
                }
                self.datePicker.delegate = self
                self.datePicker.dataSource = self
                self.date.inputView = self.datePicker
                //Init toolbar
                let toolBar = UIToolbar()
                toolBar.barStyle = UIBarStyle.Default
                toolBar.translucent = true
                toolBar.tintColor = UIColor(red: 85/255, green: 180/255, blue: 167/255, alpha: 1)
                toolBar.sizeToFit()
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddFollowTableViewCell.donePicker))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddFollowTableViewCell.canclePicker))
                
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.userInteractionEnabled = true
                self.date.inputAccessoryView = toolBar
                

            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Picker View
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return DateList.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(DateList[row])
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // selected value in Uipickerview in Swift
        date.text = String(DateList[row])
        print(row)
        selectTag = row
    }
    
    func donePicker(){
        date.resignFirstResponder()
    }
    
    func canclePicker(){
       date.resignFirstResponder()
    }
    
    
    func setSingleData(questions : [String], values: [Double]) {
        
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        for i in 0 ..< questions.count {
            yVals1.append(ChartDataEntry(value: values[i], xIndex: i))
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(yVals: yVals1, label: "First Set")
        set1.axisDependency = .Left // Line will correlate with left axis values
        set1.setColor(UIColor.redColor().colorWithAlphaComponent(0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.redColor()) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 6.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.redColor()
        set1.highlightColor = UIColor.whiteColor()
        set1.drawCircleHoleEnabled = true
        
        //3 - create an array to store our LineChartDataSets
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData = LineChartData(xVals: questions, dataSets: dataSets)
        let color = getRamdonColor()
        data.setValueTextColor(color)
        
        //5 - finally set our data
        chartView.data = data
    }
    
    @IBAction func selectBtn(sender: AnyObject) {
        let index = selectTag
        if(index != nil){
        if(self.type == "NpcParticipant" || self.type == "NpcApplicantParticipant"){
            setSingleData(questionsC, values: Lists[index!])
        }else{
            setSingleData(questionsB, values: Lists[index!])
        }
        }else{
            alert.showAlert("Error", subTitle: "Please select a date before hit the button", style: AlertStyle.Error)
        }
    }
    
    
    func setChart(dataPoints: [String], values: [[Double]]) {
        var dataSets: [LineChartDataSet] = [LineChartDataSet]()
        for i in 0..<Lists.count{
              var dataEntries: [ChartDataEntry] = []
            for j in 0..<Lists[i].count{
                let dataEntry = ChartDataEntry(value: values[i][j], xIndex: j)
                dataEntries.append(dataEntry)
            }
          let chartDataSet = LineChartDataSet(yVals:dataEntries, label: "dataset \(i)")
            let color = getRamdonColor()
            chartDataSet.setColor(color)
            chartDataSet.setCircleColor(color)
            dataSets.append(chartDataSet)
        }
        let data: LineChartData = LineChartData(xVals: dataPoints, dataSets: dataSets)
        chartView.data = data
    }
    
    
    func getRamdonColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
        typealias Completion = (success:Bool) -> Void
        func loadFollowUpData(followable_id: Int, followable_type: String,completionHandler: Completion){
            if (self.type == "NpcParticipant" || self.type == "NpcApplicantParticipant"){
            Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npc_followups/",  headers: self.dictHeader, encoding: .JSON)
                .responseJSON { response in
                    let jsonResponse = JSON(response.result.value!)
                    if(jsonResponse["error"] != nil){
                        self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
                    }
                    else{
                            for item in jsonResponse["npc_followups"].arrayValue{
                                //get user info
                                var answerList = [Double]()
                                answerList.append(item["impact_ambulation"].doubleValue)
                                answerList.append(item["impact_ambulation"].doubleValue)
                                answerList.append(item["impact_manipulation"].doubleValue)
                                answerList.append(item["impact_speech"].doubleValue)
                                answerList.append(item["impact_swallowing"].doubleValue)
                                answerList.append(item["impact_eye_movement"].doubleValue)
                                answerList.append(item["impact_seizure"].doubleValue)
                                answerList.append(item["impact_cognitive_impaired"].doubleValue)
                                answerList.append(item["impact_family_disappointed"].doubleValue)
                                answerList.append(item["impact_family_give_up"].doubleValue)
                                answerList.append(item["impact_family_worry_future"].doubleValue)
                                answerList.append(item["impact_family_closer_family"].doubleValue)
                                answerList.append(item["impact_wider_self_miss_career"].doubleValue)
                                answerList.append(item["impact_wider_family_miss_career"].doubleValue)
                                answerList.append(item["impact_wider_emergency"].doubleValue)
                                answerList.append(item["impact_wider_appointment"].doubleValue)
                                self.Lists.append(answerList)
                                self.DateList.append(self.convertToDate(item["created_at"].stringValue))
//                                self.timeList.append(item["created_at"].stringValue)
                            }
                        completionHandler(success:true)
                    }
                        }
                    }
            else if(self.type == "NpbParticipant" || self.type == "NpbApplicantParticipant"){
                Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npb_followups/",  headers: self.dictHeader, encoding: .JSON)
                    .responseJSON { response in
                        let jsonResponse = JSON(response.result.value!)
                        if(jsonResponse["error"] != nil){
                            self.alert.showAlert("Error", subTitle: jsonResponse["error"].stringValue, style: AlertStyle.Error)
                        }
                        else{
                for item in jsonResponse["npb_followups"].arrayValue{
                    //get user info
                    var answerList = [Double]()
                    answerList.append(item["symptom_bone"].doubleValue)
                    answerList.append(item["symptom_abdominal"].doubleValue)
                    answerList.append(item["symptom_breathlessness_rate"].doubleValue)
                    answerList.append(item["symptom_breathlessness_impact"].doubleValue)
                    answerList.append(item["symptom_bleeding_rate"].doubleValue)
                    answerList.append(item["symptom_bleeding_impact"].doubleValue)
                    answerList.append(item["symptom_fatigue_rate"].doubleValue)
                    answerList.append(item["symptom_fatigue_impact"].doubleValue)
                    answerList.append(item["symptom_enlarge_organ"].doubleValue)
                    answerList.append(item["symptom_enlarge_organ_impact"].doubleValue)
                    answerList.append(item["symptom_slow_growth"].doubleValue)
                    answerList.append(item["symptom_slow_growth_impact"].doubleValue)
                    answerList.append(item["symptom_infection"].doubleValue)
                    answerList.append(item["symptom_fracture"].doubleValue)
                    answerList.append(item["symptom_cognition"].doubleValue)
                    answerList.append(item["impact_family_disappointed"].doubleValue)
                    answerList.append(item["impact_family_give_up"].doubleValue)
                    answerList.append(item["impact_family_worry_future"].doubleValue)
                    answerList.append(item["impact_family_closer_family"].doubleValue)
                    answerList.append(item["impact_wider_self_miss_career"].doubleValue)
                    answerList.append(item["impact_wider_family_miss_career"].doubleValue)
                    answerList.append(item["impact_wider_emergency"].doubleValue)
                    answerList.append(item["impact_wider_appointment"].doubleValue)
                    self.Lists.append(answerList)
                    self.DateList.append(self.convertToDate(item["created_at"].stringValue))
                }
                completionHandler(success:true)
            }
        }
            }
    }
    
    
    func readJsonQuestionsChoices(){
        let url = NSBundle.mainBundle().URLForResource("follow_question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = ""
        switch type {
            
        case "NpbParticipant":
            source = "npb_question"
        case "NpbApplicantParticipant":
            source = "npb_question"
            
        case "NpcParticipant":
            source = "npc_question"
        case "NpcApplicantParticipant":
            source = "npc_applicant_participants"
        default:
            source = "npc_question"
        }
        for i in 0..<json[source].count {
            var tmpList = [String]()
            for j in 0..<json[source].arrayValue[i].count{
                tmpList.append(String(json[source][i][j]))
            }
            NumList[i] = Double(tmpList.count)
        }
    }
    
    func calculate(NumList: [Double],List: [[Double]]) -> [[Double]]{
        var newList = [[Double]]()
        var tmpList = [Double]()
        for i in 0..<List.count{
            for j in 0..<List[i].count{
                let value = (List[i][j] + 1.0) / (NumList[i] + 1.0 ) * 100
                tmpList.append(value)
            }
            newList.append(tmpList)
        }
        return newList
    }
    
    
    func convertToDate(str : String) -> String{
        let strDate = str // "2015-10-06T15:42:34Z"
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter2.timeStyle = .MediumStyle
        dateFormatter2.stringFromDate(dateFormatter.dateFromString(strDate)!)
        let str = dateFormatter2.stringFromDate(dateFormatter.dateFromString(strDate)!)
        return str
    }
  


}