//
//  showFollowUpNpbViewController.swift
//  inpdrApplication
//
//  Created by Janice on 16/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class showFollowUpNpbViewController: UIViewController {
    @IBOutlet weak var myTable: UITableView!

    var sectionTitleArray : NSMutableArray = NSMutableArray()
    var sectionContentDict : NSMutableDictionary = NSMutableDictionary()
    var arrayForBool : NSMutableArray = NSMutableArray()
    
    var dictHeader = [String: String]()

    var DicImpactInfo = NSMutableDictionary()
    var ListImpactInfo = [String]()
    var DicFamilyInfo = NSMutableDictionary()
    var ListFamilyInfo = [String]()
    var DicWiderImpact = NSMutableDictionary()
    var ListWiderImpact = [String]()
    
    var followques = followup()
    
    var dictPicker = [Int: [String]]()

    //key for searching patient
    var followable_id = 0
    
    var type = ""
    
    var id = 0
    
    //    //keep basic patient info
    //    var current_patient = patient()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readJsonQuestionsChoices()
        fetchData(self.id, followable_id: self.followable_id, followable_type: type)
        // 1 for default expand, 0 for collapse
        arrayForBool = ["1","0","0"]
        sectionTitleArray = ["ASMD NP-B Impact Questions","Impact on Family","Wider Impact"]
        var string1 = sectionTitleArray .objectAtIndex(0) as? String
        sectionContentDict.setValue(self.DicImpactInfo, forKey: string1!)
        string1 = sectionTitleArray.objectAtIndex(1) as? String
        sectionContentDict.setValue(self.DicFamilyInfo, forKey:string1! )
        string1 = sectionTitleArray.objectAtIndex(2) as? String
        sectionContentDict.setValue(self.DicWiderImpact, forKey: string1!)
        var image = UIImage(named: "line-chart.png")
        
        image = image?.imageWithRenderingMode(UIImageRenderingMode.Automatic)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(showSingleChart))
        self.myTable.registerNib(UINib(nibName: "ExpandingTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.myTable.registerNib(UINib(nibName: "ExpandingTextTableViewCell", bundle: nil), forCellReuseIdentifier: "textCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonQuestionsChoices(){
        let url = NSBundle.mainBundle().URLForResource("follow_question", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        for i in 0..<json["npb_question"].count {
            var tmpList = [String]()
            for j in 0..<json["npb_question"].arrayValue[i].count{
                tmpList.append(String(json["npb_question"][i][j]))
            }
            dictPicker[i] = tmpList
        }
    }
    
    
    func fetchData(id:Int, followable_id: Int, followable_type: String ){
        Alamofire.request(.GET, "http://localhost:3000/api/v1/\(followable_type)/\(followable_id)/npb_followups/\(id)",  headers: self.dictHeader, encoding: .JSON)
            .responseJSON { response in
                let jsonResponse = JSON(response.result.value!)
            //Parsing JSON file
            for item in jsonResponse["npb_followups"].arrayValue{
                //get followup info
                let id = item["id"].intValue
                let symptom_bone = item["symptom_bone"].intValue
                let symptom_abdominal = item["symptom_abdominal"].intValue
                let symptom_pain = item["symptom_pain"].intValue
                let symptom_breathlessness_rate = item["symptom_breathlessness_rate"].intValue
                let symptom_breathlessness_impact = item["symptom_breathlessness_impact"].intValue
                let symptom_bleeding_rate = item["symptom_bleeding_rate"].intValue
                let symptom_bleeding_impact = item["symptom_bleeding_impact"].intValue
                let symptom_fatigue_rate = item["symptom_fatigue_rate"].intValue
                let symptom_fatigue_impact = item["symptom_fatigue_impact"].intValue
                let symptom_enlarge_organ = item["symptom_enlarge_organ"].intValue
                let symptom_enlarge_organ_impact = item["symptom_enlarge_organ_impact"].intValue
                let symptom_slow_growth = item["symptom_slow_growth"].intValue
                let symptom_slow_growth_impact = item["symptom_slow_growth_impact"].intValue
                let symptom_infection = item["symptom_infection"].intValue
                let symptom_fracture = item["symptom_fracture"].intValue
                let symptom_cognition = item["symptom_cognition"].intValue
                let impact_family_disappointed = item["impact_family_disappointed"].intValue
                let impact_family_give_up = item["impact_family_give_up"].intValue
                let impact_family_worry_future = item["impact_family_worry_future"].intValue
                let impact_family_closer_family = item["impact_family_closer"].intValue
                let impact_wider_self_miss_career = item["impact_wider_self_miss_career"].intValue
                let impact_wider_carer_miss_career = item["impact_wider_carer_miss_career"].intValue
                let impact_wider_emergency = item["impact_wider_emergency"].intValue
                let impact_wider_appointment = item["impact_wider_appointment"].intValue
                let created_at = item["created_at"].stringValue
                let updated_at = item["updated_at"].stringValue
                let followable_id = item["followable_id"].intValue
                let followable_type = item["followable_type"].stringValue
                
                let current_npb = npb_followup()
                current_npb.id = id
                current_npb.symptom_bone = symptom_bone
                current_npb.symptom_abdominal = symptom_abdominal
                current_npb.symptom_pain = symptom_pain
                current_npb.symptom_breathlessness_rate = symptom_breathlessness_rate
                current_npb.symptom_breathlessness_impact = symptom_breathlessness_impact
                current_npb.symptom_bleeding_rate = symptom_bleeding_rate
                current_npb.symptom_bleeding_impact = symptom_bleeding_impact
                current_npb.symptom_fatigue_rate = symptom_fatigue_rate
                current_npb.symptom_fatigue_impact = symptom_fatigue_impact
                current_npb.symptom_enlarge_organ = symptom_enlarge_organ
                current_npb.symptom_enlarge_organ_impact = symptom_enlarge_organ_impact
                current_npb.symptom_slow_growth = symptom_slow_growth
                current_npb.symptom_slow_growth_impact = symptom_slow_growth_impact
                current_npb.symptom_infection = symptom_infection
                current_npb.symptom_fracture = symptom_fracture
                current_npb.symptom_cognition = symptom_cognition
                current_npb.impact_family_disappointed = impact_family_disappointed
                current_npb.impact_family_give_up = impact_family_give_up
                current_npb.impact_family_worry_future = impact_family_worry_future
                current_npb.impact_family_closer = impact_family_closer_family
                current_npb.impact_wider_self_miss_career = impact_wider_self_miss_career
                current_npb.impact_wider_carer_miss_career = impact_wider_carer_miss_career
                current_npb.impact_wider_emergency = impact_wider_emergency
                current_npb.impact_wider_appointment = impact_wider_appointment
                current_npb.created_at = created_at
                current_npb.updated_at = updated_at
                current_npb.followable_id = followable_id
                current_npb.followable_type = followable_type
                //                self.npb_follows.append(current_npb)
                
                self.readJson()
                self.configTable(current_npb)
                
            }
            self.myTable.reloadData()
        }
    }
    
    func showSingleChart(){
        let List = [DicImpactInfo.allValues, DicFamilyInfo.allValues, DicWiderImpact.allValues].reduce([],combine:+)
        var newList = [Double]()
        for i in 0..<List.count{
            newList.append(Double(List[i] as! NSNumber))
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("singleChart") as! SingleChartViewController
        vc.Lists = newList
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func readJson(){
        let url = NSBundle.mainBundle().URLForResource("followup", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        let json = JSON(data: data!)
        var source = "npb_follow"
        if(type == "NpbApplicantParticipant"){
            source = "npbA_follow"
        }
        for item in json[source].arrayValue{
            self.followques.bone_pain = item["symptom_bone"].stringValue
            self.followques.abdominal = item["symptom_abdominal"].stringValue
            self.followques.pain_impact_rate = item["symptom_pain"].stringValue
            self.followques.breath = item["symptom_breathlessness_rate"].stringValue
            self.followques.breath_rate = item["symptom_breathlessness_impact"].stringValue
            self.followques.bleed = item["symptom_bleeding_rate"].stringValue
            self.followques.bleed_rate = item["symptom_bleeding_impact"].stringValue
            self.followques.tired = item["symptom_fatigue_rate"].stringValue
            self.followques.fatigue_impact = item["symptom_fatigue_impact"].stringValue
            self.followques.organ_enlarge = item["symptom_enlarge_organ"].stringValue
            self.followques.organ_rate = item["symptom_enlarge_organ_impact"].stringValue
            self.followques.slow_groth = item["symptom_slow_growth"].stringValue
            self.followques.slow_rate = item["symptom_slow_growth_impact"].stringValue
            self.followques.infection = item["symptom_infection"].stringValue
            self.followques.fractures = item["symptom_fracture"].stringValue
            self.followques.cognitive = item["symptom_cognition"].stringValue
            
            self.followques.family_disapponited = item["impact_family_disappointed"].stringValue
            self.followques.family_giveup = item["impact_family_give_up"].stringValue
            self.followques.family_worry = item["impact_family_worry_future"].stringValue
            self.followques.close_family = item["impact_family_closer"].stringValue
            self.followques.wider_miss = item["impact_wider_self_miss_career"].stringValue
            self.followques.wider_carer = item["impact_wider_carer_miss_career"].stringValue
            self.followques.emergency = item["impact_wider_emergency"].stringValue
            self.followques.appointment = item["impact_wider_appointment"].stringValue        }
    }
    func configTable(follow: npb_followup){
        
        //config List1 Q1
        self.DicImpactInfo["0"] = follow.symptom_bone
        self.DicImpactInfo["1"] = follow.symptom_abdominal
        self.DicImpactInfo["2"] = follow.symptom_pain
        self.DicImpactInfo["3"] = follow.symptom_breathlessness_rate
        self.DicImpactInfo["4"] = follow.symptom_breathlessness_impact
        self.DicImpactInfo["5"] = follow.symptom_bleeding_rate
        self.DicImpactInfo["6"] = follow.symptom_bleeding_impact
        self.DicImpactInfo["7"] = follow.symptom_fatigue_rate 
        self.DicImpactInfo["8"] = follow.symptom_fatigue_impact
        self.DicImpactInfo["9"] = follow.symptom_enlarge_organ
        self.DicImpactInfo["10"] = follow.symptom_enlarge_organ_impact
        self.DicImpactInfo["11"] = follow.symptom_slow_growth
        self.DicImpactInfo["12"] = follow.symptom_slow_growth_impact
        self.DicImpactInfo["13"] = follow.symptom_infection
        self.DicImpactInfo["14"] = follow.symptom_fracture
        self.DicImpactInfo["15"] = follow.symptom_cognition
        self.ListImpactInfo = [followques.bone_pain,followques.abdominal,followques.pain_impact_rate,followques.breath,followques.breath_rate,followques.bleed,followques.bleed_rate,followques.tired,followques.fatigue_impact,followques.organ_enlarge,followques.organ_rate,followques.slow_groth,followques.slow_rate,followques.infection,followques.fractures,followques.cognitive]
        //config List2 Q1
        self.DicFamilyInfo["0"] = follow.impact_family_disappointed
        self.DicFamilyInfo["1"] = follow.impact_family_give_up
        self.DicFamilyInfo["2"] = follow.impact_family_worry_future
        self.DicFamilyInfo["3"] = follow.impact_family_closer
        
        
        self.ListFamilyInfo = [followques.family_disapponited,followques.family_giveup,followques.family_worry,followques.close_family]
        
        //config List3 Q1
        self.ListWiderImpact = [followques.wider_miss,followques.wider_carer,followques.emergency,followques.appointment]
        //config List3
        self.DicWiderImpact["0"] = follow.impact_wider_self_miss_career
        self.DicWiderImpact["1"] = follow.impact_wider_carer_miss_career
        self.DicWiderImpact["2"] = follow.impact_wider_emergency
        self.DicWiderImpact["3"] = follow.impact_wider_appointment
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitleArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(arrayForBool .objectAtIndex(section).boolValue == true)
        {
            let tps = sectionTitleArray.objectAtIndex(section) as! String
            let count1 = (sectionContentDict.valueForKey(tps)?.allValues) as! NSArray
            return count1.count
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "ABC"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(arrayForBool.objectAtIndex(indexPath.section).boolValue == true){
            //            if(indexPath.section == 1 && indexPath.row == 2){return 400}
            //            else if(indexPath.section == 2 && indexPath.row == 1){
            //                return 250
            //            }
            return 120
        }
        
        return 3;
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        headerView.backgroundColor = UIColor(red: 255/255, green: 250/255, blue: 244/255, alpha: 1)
        headerView.tag = section
        
        let headerString = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.frame.size.width-10, height: 30)) as UILabel
        headerString.text = sectionTitleArray.objectAtIndex(section) as? String
        headerString.textColor = UIColor(red: 207/255, green: 165/255, blue: 164/255, alpha: 1)
        headerView .addSubview(headerString)
        
        let headerTapped = UITapGestureRecognizer (target: self, action:#selector(ExpandingViewController.sectionHeaderTapped(_:)))
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView
    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        
        let indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        if (indexPath.row == 0) {
            
            var collapsed = arrayForBool.objectAtIndex(indexPath.section).boolValue
            collapsed = !collapsed;
            
            arrayForBool .replaceObjectAtIndex(indexPath.section, withObject: collapsed)
            //reload specific section animated
            let range = NSMakeRange(indexPath.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.myTable .reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //        let index = indexPath.section
        if(sectionTitleArray.objectAtIndex(indexPath.section) as! String == "ASMD NP-B Impact Questions"){
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                var tmpList = [Int]()
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                for key in keys{
                    tmpList.append(Int(key)!)
                }
                let sorted = tmpList.sort()
                print(sorted)
                cell.fieldLbl.text = ListImpactInfo[Int(sorted[indexPath.row])]
                print(cell.fieldLbl.text)
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])]!
                let ind = DicImpactInfo.valueForKey(String(sorted[indexPath.row]))! as! Int
                cell.contentLbl.text = strs[ind]
            }
            return cell
        }
        else if (sectionTitleArray.objectAtIndex(indexPath.section) as! String == "Impact on Family"){
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListFamilyInfo[Int(sorted[indexPath.row])!]
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])! + 16]!
                let ind = DicFamilyInfo.valueForKey(sorted[indexPath.row])! as! Int
                cell.contentLbl.text = strs[ind]

            }
            return cell
            
        }else{
            let cellIdentifier = "Cell"
            let cell = self.myTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ExpandingTableViewCell
            
            
            let manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
            
            if (!manyCells) {
                //              cell.textLabel!.text = "click to enlarge";
            }
            else{
                let keys = (sectionContentDict.valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String)?.allKeys)! as! [String]
                let sorted = keys.sort()
                cell.fieldLbl.text = ListWiderImpact[Int(sorted[indexPath.row])!]
                var strs = [String]()
                strs = dictPicker[Int(sorted[indexPath.row])! + 20]!
                let ind = DicWiderImpact.valueForKey(sorted[indexPath.row])! as! Int
                cell.contentLbl.text = strs[ind]

                
            }
            return cell
        }
    }
}

