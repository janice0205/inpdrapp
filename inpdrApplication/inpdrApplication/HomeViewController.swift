//
//  HomeViewController.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let alert = SweetAlert()
    @IBOutlet weak var myTable: UITableView!
    
    var patients = [patient]()
    var users = [user]()
    var dictHeader = [String: String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarController = self.tabBarController as! MyTabBarController
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log out", style: .Plain, target: self, action: #selector(logOut))
        patients = tabBarController.patients
        users = tabBarController.users
        dictHeader = tabBarController.dictHeader
        self.myTable.reloadData()
        myTable.delegate = self
        myTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return patients.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(otableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let current_patient = patients[indexPath.row]
        var cell = myTable.dequeueReusableCellWithIdentifier("patientCell") as! PatientTableViewCell
               if(cell.identifier == true){
            cell = myTable.dequeueReusableCellWithIdentifier("patientCell") as! PatientTableViewCell
        }
        cell.followupBtn.addTarget(self, action: #selector(showFollowup), forControlEvents: UIControlEvents.TouchUpInside)
        let type = current_patient.enrollable_type
        if (current_patient.verified == false){
            cell.followupBtn.enabled = false
        }
        var unique_id = current_patient.unique_id
        switch type {
        case "NpaApplicantParticipant":
            unique_id = "NPA-"+unique_id
            cell.viewBtn.addTarget(self, action: #selector(viewDetailActionNPA), forControlEvents: .TouchUpInside)
            cell.followupBtn.enabled = false
            cell.followupBtn.setTitle("In development", forState: .Normal)

        case "NpbParticipant":
            unique_id = "NPB-"+unique_id
            cell.viewBtn.addTarget(self, action: #selector(viewDetailActionNPB), forControlEvents: .TouchUpInside)
        case "NpbApplicantParticipant":
            unique_id = "NPB-"+unique_id
            cell.viewBtn.addTarget(self, action: #selector(viewDetailActionNPBA), forControlEvents: .TouchUpInside)
        case "NpcParticipant":
            unique_id = "NPC-"+unique_id
            cell.viewBtn.addTarget(self, action: #selector(viewDetailActionNPC), forControlEvents: .TouchUpInside)
        case "NpcApplicantParticipant":
            unique_id = "NPC-"+unique_id
            cell.viewBtn.addTarget(self, action: #selector(viewDetailActionNPCA), forControlEvents: .TouchUpInside)
        default:
            unique_id = "unknown-"+unique_id
            cell.viewBtn.enabled = false
            cell.followupBtn.enabled = false
        }
       
        cell.patientTypelbl.text = type
        cell.patientIDlbl.text = unique_id
        cell.viewBtn.tag = indexPath.row
        cell.followupBtn.tag = indexPath.row
        cell.identifier = true
        
        return cell
    }
    
    
    func viewDetailActionNPA(sender: UIButton){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("expandingNpaVC") as! ExpandingNpaViewController
        vc.patientID = patients[sender.tag].enrollable_id
        vc.dictHeader = self.dictHeader
        vc.current_patient = patients[sender.tag]
        vc.navigationItem.title = "Patient Details"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func viewDetailActionNPB(sender: UIButton){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("expandingNpbVC") as! ExpandingNpbViewController
        vc.patientID = patients[sender.tag].enrollable_id
        vc.dictHeader = self.dictHeader
        vc.current_patient = patients[sender.tag]
        vc.navigationItem.title = "Patient Details"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewDetailActionNPBA(sender: UIButton){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("expandingNpbaVC") as! ExpandingNpbaViewController
        vc.patientID = patients[sender.tag].enrollable_id
        vc.dictHeader = self.dictHeader
        vc.current_patient = patients[sender.tag]
        vc.navigationItem.title = "Patient Details"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    func viewDetailActionNPC(sender: UIButton){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("expandingVC") as! ExpandingViewController
        vc.patientID = patients[sender.tag].enrollable_id
        vc.dictHeader = self.dictHeader
        vc.current_patient = patients[sender.tag]
        vc.navigationItem.title = "Patient Details"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewDetailActionNPCA(sender: UIButton){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("expandingNpcaVC") as! ExpandingNpcaViewController
        vc.patientID = patients[sender.tag].enrollable_id
        vc.dictHeader = self.dictHeader
        vc.current_patient = patients[sender.tag]
        vc.navigationItem.title = "Patient Details"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showFollowup(sender: UIButton){
        let type = patients[sender.tag].enrollable_type
        let enrollable_id = patients[sender.tag].enrollable_id
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("followupVC") as! FollowUpViewController
        vc.type = type
        vc.dictHeader = self.dictHeader
        vc.enrollable_id = enrollable_id
        vc.navigationItem.title = "FollowUps"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func logOut(){
        SweetAlert().showAlert("Are you sure?", subTitle: "You will be logged out from app", style: AlertStyle.Warning, buttonTitle:"No, cancel plx!", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "Yes, do it!", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                SweetAlert().showAlert("Cancelled!", subTitle: "", style: AlertStyle.Error)
            }
            else {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("logview") as! LogInViewController
        self.presentViewController(vc, animated: true, completion: nil)        //        let navigationController =
            }
 
        }
    }
    
}
