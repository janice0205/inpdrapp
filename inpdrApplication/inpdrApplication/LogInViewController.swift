//
//  LogInViewController.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import KeychainAccess
class LogInViewController: UIViewController {
    
   
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var pwdTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    
    let alert = SweetAlert()
    
    var dictHeader = [String: String]()
    
    var patients = [patient]()
    var users = [user]()

    override func viewDidLoad() {
        //debug
//        let keychain = Keychain(service: "authentication")
        
//        if keychain.allKeys().count != 0 {
//            self.emailTxt.text = keychain.get("email")
//            self.pwdTxt.text = keychain.get("password")
//        }
        
        self.emailTxt.text = "siyuan.zhan25@gmail.com"
        self.pwdTxt.text = "password1"
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchAuthCode(email: String, password: String){
        var dictAuth = [String: AnyObject]()
        dictAuth["email"] = email
        dictAuth["password"] = password
        let parameter = dictAuth
        let url = "http://localhost:3000/api/login"
        
        Alamofire.request(.POST, url, parameters: parameter, encoding: .JSON).responseJSON { response in
            var json = JSON(response.result.value!)
            if(json["status"] == "ok"){
                self.dictHeader["X-Auth-Token"] = json["token"].stringValue
                print("saved")
                let keychain = Keychain(service: "authentication")
                do {
                    try keychain.remove("authentication")
                } catch let error {
                    print("error: \(error)")
                }
           
                keychain["email"] = email
                keychain["password"] = password
                
                Alamofire.request(.GET, "http://localhost:3000/api/v1/patients", headers: self.dictHeader, encoding: .JSON)
                    .responseJSON { response in
                        let jsonResponse = JSON(response.result.value!)
                        for item in jsonResponse["patients"].arrayValue{
                        //get user info
                        let user_id = item["user"]["id"].intValue
                        let user_email = item["user"]["email"].stringValue
                        let current_user = user(id: user_id, email: user_email)
                        self.users.append(current_user!)
                        //get patient info
                        let id = item["id"].intValue
                        let registrant_first_name = item["registrant_first_name"].stringValue
                        let registrant_last_name = item["registrant_last_name"].stringValue
                        let relation_to_patient = item["relation_to_patient"].stringValue
                        let addresses_id = item["addresses_id"].intValue
                        let patient_first_name = item["patient_first_name"].stringValue
                        let patient_last_name = item["patient_last_name"].stringValue
                        let gender = item["gender"].intValue
                        let dob = item["date_of_birth"].stringValue
                        let consent = item["consent"].boolValue
                        let consent_name = item["consent_name"].stringValue
                        let verified = item["verified"].boolValue
                        let unique_id = item["unique_id"].stringValue
                        let enrollable_id = item["enrollable_id"].intValue
                        let enrollable_type = item["enrollable_type"].stringValue
                        let not_finish = item["not_finish"].boolValue
        
                        let current_patient = patient()
                        current_patient.id = id
                        current_patient.registrant_first_name = registrant_first_name
                        current_patient.registrant_last_name = registrant_last_name
                        current_patient.relation_to_patient = relation_to_patient
                        current_patient.addresses_id = addresses_id
                        current_patient.patient_first_name = patient_first_name
                        current_patient.patient_last_name = patient_last_name
                        current_patient.gender = gender
                        current_patient.date_of_birth = dob
                        current_patient.user_id = user_id
                        current_patient.consent = consent
                        current_patient.consent_name = consent_name
                        current_patient.verified = verified
                        current_patient.unique_id = unique_id
                        current_patient.enrollable_id = enrollable_id
                        current_patient.enrollable_type = enrollable_type
                        current_patient.not_finish = not_finish
                        
                        self.patients.append(current_patient)

                }
                self.performSegueWithIdentifier("enterHome", sender: self)
                        
            }
            }else{
                self.alert.showAlert("Error", subTitle: "Please check you email and password", style: AlertStyle.Error)
            }
        }
        
    }
    
    @IBAction func submitBtn(sender: AnyObject) {
        fetchAuthCode(self.emailTxt.text!, password: self.pwdTxt.text!)
        if (mySwitch.on){
           
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "enterHome") {
            let tabBarController = segue.destinationViewController as! MyTabBarController
            tabBarController.patients = self.patients
            tabBarController.users = self.users
            tabBarController.dictHeader = self.dictHeader
        }
    }
    
}