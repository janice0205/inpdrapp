//
//  npc_followup.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class npc_followup{
    
    var id = 0
    var impact_ambulation = 0
    var impact_manipulation = 0
    var impact_speech = 0
    var impact_swallowing = 0
    var impact_eye_movement = 0
    var impact_seizure = 0
    var impact_cognitive_impaired = 0
    var impact_family_disappointed = 0
    var impact_family_give_up = 0
    var impact_family_worry_future = 0
    var impact_family_closer = 0
    var impact_wider_self_miss_career = 0
    var impact_wider_family_miss_career = 0
    var impact_wider_emergency = 0
    var impact_wider_appointment = 0
    var created_at = ""
    var updated_at = ""
    var followable_id = 0
    var followable_type = ""
    
    
//    init?(impact_ambulation:Int,impact_manipulation:Int,impact_speech:Int,impact_swallowing:Int,impact_eye_movement:Int,impact_seizure:Int,impact_cognitive_impaired:Int,impact_family_disappointed:Int,impact_family_give_up:Int,impact_family_worry_future:Int,impact_family_closer:Int,impact_wider_self_miss_career:Int,impact_wider_carer_miss_career:Int,impact_wider_emergency:Int,impact_wider_appointment:Int,followable_id:Int,followable_type:String){
//        self.impact_ambulation = impact_ambulation
//        self.impact_manipulation = impact_manipulation
//        self.impact_speech = impact_speech
//        self.impact_swallowing = impact_swallowing
//        self.impact_eye_movement = impact_eye_movement
//        self.impact_seizure = impact_seizure
//        self.impact_cognitive_impaired = impact_cognitive_impaired
//        self.impact_family_disappointed = impact_family_disappointed
//        self.impact_family_give_up = impact_family_give_up
//        self.impact_family_worry_future = impact_family_worry_future
//        self.impact_family_closer = impact_family_closer
//        self.impact_wider_self_miss_career = impact_wider_self_miss_career
//        self.impact_wider_carer_miss_career = impact_wider_carer_miss_career
//        self.impact_wider_emergency = impact_wider_emergency
//        self.impact_wider_appointment = impact_wider_appointment
//        self.followable_id = followable_id
//        self.followable_type = followable_type
//        
//    }
    
}