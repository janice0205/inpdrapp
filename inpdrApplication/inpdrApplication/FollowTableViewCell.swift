//
//  FollowTableViewCell.swift
//  inpdrApplication
//
//  Created by Janice on 15/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class FollowTableViewCell: UITableViewCell {

    @IBOutlet weak var destroyBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var showBtn: UIButton!
    @IBOutlet weak var created_at: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
