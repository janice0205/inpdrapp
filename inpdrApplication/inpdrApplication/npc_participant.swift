//
//  npc_participant.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class npc_participant{
    var id = 0
    var age_diagnosed_known = false
    var age_diagnosed_year = 0
    var age_diagnosed_month = 0
    var family_diagnosed = 0
    var family_diagnosed_relation = ""
    var hospital_name = ""
    var hospital_address = ""
    var hospital_email = ""
    var gene_test_performed = false
    var baby_symptoms = 0
    var symptom_age_year = 0
    var symptom_age_month = 0
    var symptom_age_unknown = false
    var symptom_age_na = false
    var symptom_delayed_milestone = false
    var symptom_ppers = false
    var symptom_coordication = false
    var symptom_eye = false
    var symptom_behavioural = false
    var symptom_seizure = false
    var symptom_psychiatric = false
    var symptom_other = false
    var symptom_other_specify = ""
    var hospital_clinician_name = ""
    var gene_diagnosis = ""
    var unable_to_contact_specialist = false
    var not_finish = false

//    var id : Int
//    var age_diagnosed_known: Bool
//    var age_diagnosed_year : Int
//    var age_diagnosed_month : Int
//    var family_diagnosed : Int
//    var family_diagnosed_relation : String
//    var hospital_name : String
//    var hospital_address : String
//    var hospital_email : String
//    var gene_test_performed : Bool
//    var baby_symptoms : Int
//    var symptom_age_year : Int
//    var symptom_age_month : Int
//    var symptom_age_unknown : Bool
//    var symptom_age_na : Bool
//    var symptom_delayed_milestone : Bool
//    var symptom_ppers : Bool
//    var symptom_coordication: Bool
//    var symptom_eye : Bool
//    var symptom_behavioural: Bool
//    var symptom_seizure: Bool
//    var symptom_psychiatric: Bool
//    var symptom_other : Bool
//    var symptom_other_specify: String
//    var hospital_clinician_name : String
//    var gene_diagnosis : String
//    var unable_to_contact_specialist:Bool
//    var not_finish:Bool
//
//    init?(id: Int, age_diagnosed_known:Bool, age_diagnosed_year: Int, age_diagnosed_month:Int, family_diagnosed: Int,family_diagnosed_relation:String,hospital_name:String,hospital_address:String,hospital_email:String,gene_test_performed:Bool,baby_symptoms: Int,symptom_age_year:Int,symptom_age_month:Int,symptom_age_unknown:Bool,symptom_age_na:Bool,symptom_delayed_milestone: Bool,symptom_ppers: Bool, symptom_coordication: Bool, symptom_eye: Bool,symptom_behavioural: Bool,symptom_seizure: Bool, symptom_psychiatric: Bool, symptom_other: Bool, symptom_other_specify: String,hospital_clinician_name:String,gene_diagnosis: String,unable_to_contact_specialist:Bool,not_finish:Bool){
//        self.id = id
//        self.age_diagnosed_known = age_diagnosed_known
//        self.age_diagnosed_year = age_diagnosed_year
//        self.age_diagnosed_month = age_diagnosed_month
//        self.family_diagnosed = family_diagnosed
//        self.family_diagnosed_relation = family_diagnosed_relation
//        self.hospital_name = hospital_name
//        self.hospital_address = hospital_address
//        self.hospital_email = hospital_email
//        self.gene_test_performed = gene_test_performed
//        self.baby_symptoms = baby_symptoms
//        self.symptom_age_year = symptom_age_year
//        self.symptom_age_month = symptom_age_month
//        self.age_diagnosed_known = age_diagnosed_known
//        self.symptom_age_unknown = symptom_age_unknown
//        self.symptom_age_na = symptom_age_na
//        self.symptom_delayed_milestone = symptom_delayed_milestone
//        self.symptom_ppers = symptom_ppers
//        self.symptom_coordication =  symptom_coordication
//        self.symptom_eye = symptom_eye
//        self.symptom_behavioural = symptom_behavioural
//        self.symptom_seizure = symptom_seizure
//        self.symptom_psychiatric = symptom_psychiatric
//        self.symptom_other = symptom_other
//        self.symptom_other_specify = symptom_other_specify
//        self.hospital_clinician_name = hospital_clinician_name
//        self.gene_diagnosis = gene_diagnosis
//        self.unable_to_contact_specialist = unable_to_contact_specialist
//        self.not_finish = not_finish
//    }
}