//
//  npb_applicant_participant.swift
//  inpdrApplication
//
//  Created by Janice on 2/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import Foundation
class npb_applicant_participant{
    
    var id = 0
    var age_diagnosed_known = false
    var age_diagnosed_year = 0
    var age_diagnosed_month = 0
    var family_diagnosed = 0
    var family_diagnosed_relation = ""
    var applicant_relation = 0
    var applicant_relation_specify = ""
    var gene_test_performed = false
    var symptom_age_year = 0
    var symptom_age_month = 0
    var symptom_age_unknown = false
    var symptom_age_na = false
    var symptom_enlarged_liver = false
    var symptom_breathing = false
    var symptom_anaemia = false
    var symptom_bleeding = false
    var symptom_growth_delay = false
    var symptom_developmental_delay = false
    var symptom_bone_pain = false
    var symptom_abdominal_pain = false
    var symptom_other = false
    var symptom_other_specify = ""
    var hospital_clinician_name = ""
    var gene_diagnosis = ""
    var unable_to_contact_specialist = false
    var not_finish = false
    
    
//    init?(id: Int,  age_diagnosed_known:Bool, age_diagnosed_year: Int, age_diagnosed_month:Int, family_diagnosed: Int,family_diagnosed_relation:String,applicant_relation:Int, applicant_relation_specify:String, hospital_name:String,hospital_address:String,hospital_email:String,gene_test_performed:Bool,symptom_age_year:Int,symptom_age_month:Int,symptom_age_unknown:Bool,symptom_age_na:Bool,symptom_enlarged_liver: Bool,symptom_breathing: Bool, symptom_anaemia: Bool, symptom_bleeding: Bool,symptom_growth_delay: Bool,symptom_developmental_delay: Bool, symptom_bone_pain: Bool, symptom_abdominal_pain: Bool, symptom_other: Bool, symptom_other_specify: String,hospital_clinician_name:String, gene_diagnosis: String,unable_to_contact_specialist:Bool,not_finish:Bool){
//        self.id = id
//        self.age_diagnosed_known = age_diagnosed_known
//        self.age_diagnosed_year = age_diagnosed_year
//        self.age_diagnosed_month = age_diagnosed_month
//        self.family_diagnosed = family_diagnosed
//        self.family_diagnosed_relation = family_diagnosed_relation
//        self.applicant_relation = applicant_relation
//        self.applicant_relation_specify = applicant_relation_specify
//        self.gene_test_performed = gene_test_performed
//        self.symptom_age_year = symptom_age_year
//        self.symptom_age_month = symptom_age_month
//        self.age_diagnosed_known = age_diagnosed_known
//        self.symptom_age_unknown = symptom_age_unknown
//        self.symptom_age_na = symptom_age_na
//        self.symptom_enlarged_liver = symptom_enlarged_liver
//        self.symptom_breathing = symptom_breathing
//        self.symptom_anaemia =  symptom_anaemia
//        self.symptom_bleeding = symptom_bleeding
//        self.symptom_growth_delay = symptom_growth_delay
//        self.symptom_developmental_delay = symptom_developmental_delay
//        self.symptom_bone_pain = symptom_bone_pain
//        self.symptom_abdominal_pain = symptom_abdominal_pain
//        self.symptom_other = symptom_other
//        self.symptom_other_specify = symptom_other_specify
//        self.hospital_clinician_name = hospital_clinician_name
//        self.gene_diagnosis = gene_diagnosis
//        self.unable_to_contact_specialist = unable_to_contact_specialist
//        self.not_finish = not_finish
//    }
}