//
//  AddFollowFamilyViewController.swift
//  inpdrApplication
//
//  Created by Janice on 17/05/2016.
//  Copyright © 2016 Siyuan. All rights reserved.
//

import UIKit

class AddFollowFamilyViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var dictHeader = [String: String]()

    var selectedAnswerForRow: [Int:String] = [:]
    
    var alert = SweetAlert()
    
    @IBOutlet weak var myTable: UITableView!
    
    var dictPicker = [Int: [String]]()
    var startIndex: Int?
    var listQuestion2 = [String]()
    var listQuestion3 = [String]()
    var followques = followup()
    var dictAnswer = [Int : Int]()
    var followable_id : Int?
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myTable.registerNib(UINib(nibName: "AddFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "addFollowCell")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: #selector(addWider))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Tableview
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return listQuestion2.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = myTable.dequeueReusableCellWithIdentifier("addFollowCell") as! AddFollowTableViewCell
        if(cell.identifier == true){
            cell.answerText.text = selectedAnswerForRow[indexPath.row]
        }
        cell.questionView.text = listQuestion2[indexPath.row]
        cell.pickerDataSource = dictPicker[indexPath.row + startIndex!]!
        dictAnswer[indexPath.row + startIndex!] = cell.pickerValue
        cell.answerText.addTarget(self, action: #selector(AddFollowUpViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingDidEnd)
        cell.answerText.tag = indexPath.row
        cell.identifier = true
        return cell
    }
    
    func textFieldDidChange(sender: UITextField){
        let cell = sender.superview?.superview as! AddFollowTableViewCell
        dictAnswer[sender.tag + startIndex!] = cell.pickerValue
        let  rowIndex = myTable.indexPathForCell(cell)?.row
        selectedAnswerForRow[rowIndex!] = cell.answerValue
    }
    
    func addWider(){
        print("addWider")
        print(dictAnswer)
        if(dictAnswer.values.contains(-1)){
            alert.showAlert("Error", subTitle: "Please fill all questions", style: AlertStyle.Error)
        }else{
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("addWider") as! AddFollowWiderViewController
        vc.dictHeader = dictHeader
        vc.dictPicker = self.dictPicker
        vc.startIndex = self.startIndex! + self.listQuestion2.count
        vc.listQuestion3 = self.listQuestion3
        vc.followques = followques
        vc.navigationItem.title = "Wider Impact"
        vc.dictAnswer = dictAnswer
        vc.followable_id = followable_id
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    }
}
